<?php

// Page IDs
define('TermsofUsePageID', 43);
define('PrivacyPolicyPageID', 45);
define('StoriesPageID', 19);
define('Home', 5);
define('Heroes', 96);
define('Diana', 83);
define('Allen', 85);
define('Margaret', 87);
define('Mike', 89);
define('Tracy', 91);
define('Morgan', 93);


// Categories
define('uncategorizedCategoryID', 1);


// Heroes Content
$diana = array( 'h', array('Honesty', 'Healing', 'Hope'), '#a4a35c', '#7291b0', 83, 'home', 'allen', 'In order for <strong>healing</strong> to begin, <strong>hope</strong> must first be restored. The first step in the journey begins by creating a safe haven, a sense of security and a place where people can be <strong>honest</strong> without fear of judgment.' );
$allen = array( 'e', array('Engage', 'Embrace', 'Empower'), '#7291b0', '#c58634', 100, 'diana', 'margaret', 'Feelings are powerful. By <strong>engaging</strong> our emotions we begin to understand others - and ourselves. <strong>Embracing</strong> emotions is <strong>empowering</strong>; it enables us to recognize how our motivations and behaviors directly affect our lives.' );
$margaret = array( 'r', array('Receive', 'Renewal', 'Restore'), '#c58634', '#a4a35c', 87, 'allen', 'mike', 'With mental and emotional barriers removed, we are finally able to <strong>receive</strong> a message of help and healing. As we begin to fully realize the power of self-governance and control over our behavior and choices, transformation occurs. This period of radical acceptance and self-rediscovery opens the door to <strong>renewal</strong> of spirit and <strong>restoration</strong> of hope.' );
$mike = array( 'o', array('Optimism', 'Openness', 'Overcome'), '#5998a4', '#c58634', 89, 'margaret', 'tracy', 'With empowerment and hope comes a restored sense of <strong>optimism</strong> and possibility for the future. Equipped with a greater sense of self-awareness, deeper understanding of our motivations and <strong>open</strong> heart, we move beyond the facility equipped with the tools to <strong>overcome</strong> the challenges and obstacles that await.' );
$tracy = array( 'ee', array('Expand', 'Evolve', 'Explore'), '#d35c34', '#bd982e', 91, 'mike', 'morgan', 'We leave the security of a healing haven having <strong>evolved</strong> and transformed from the person we were upon arrival. Departing with a healthy sense of anxiety and anticipation about the future, we are now ready to <strong>expand</strong> on what we have learned by <strong>exploring</strong> and enjoying life in recovery.' );
$morgan = array( 's', array('Sustain', 'Success', 'Serve'), '#a4a35c', '#7291b0', 93, 'tracy', null, 'Each day is an opportunity to <strong>sustain</strong> and celebrate the <strong>success</strong> of living in recovery. By sharing our story and <strong>serving</strong> others, we connect with other people, creating and contributing to an essential support system.' );

$the_heroes = array('diana', 'allen', 'margaret', 'mike', 'tracy', 'morgan');

// Journey Content
$diana_story = array( 'h', '#a4a35c', '#7291b0', 'home', 'allen' );
$allen_story = array( 'e', '#7291b0', '#c58634', 'diana', 'margaret' );
$margaret_story = array( 'r', '#c58634', '#a4a35c', 'allen', 'mike' );
$mike_story = array( 'o', '#5998a4', '#c58634', 'margaret', 'tracy' );
$tracy_story = array( 'ee', '#d35c34', '#bd982e', 'mike', 'morgan' );
$morgan_story = array( 's', '#a4a35c', '#7291b0', 'tracy', 'home' );
?>