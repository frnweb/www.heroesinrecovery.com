<?php
/*
Template Name: Help Template
*/
?>

<?php get_header(); ?>

<div id="main">

	<div id="contact" class="contact">
	
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<h3>We want to hear from you.</h3>
		
		<div class="contact-container">
			
			<div class="call-map">
				<div class="phone">Call Us<br /><span id="frn_phones" ga_phone_location="Phone Clicks in Page (Contact Us)" style="white-space:nowrap;" >(888) 312-4220</span></div>
				<div class="map">
					<p>Find Us</p>
					Heroes in Recovery<br />
					c/o Foundations Recovery Network<br />
					5409 Maryland Way, Suite 320<br />
					Brentwood, TN 37027
					
					<div class="map-frame">
						<iframe width='400' height='300' frameBorder='0' src='http://a.tiles.mapbox.com/v3/incrowdmedia.map-5zvg0izv.html#15/36.03725210887611/-86.81702883605955'></iframe>
					</div>
				</div>
			</div>
			
			<div class="form">
				<div style="padding:5px 10px;">
				<p>Email Us</p>
				<script type="text/javascript">var host = (("https:" == document.location.protocol) ? "https://secure." : "http://");document.write(unescape("%3Cscript src='" + host + "wufoo.com/scripts/embed/form.js' type='text/javascript'%3E%3C/script%3E"));</script>

				<script type="text/javascript">
				var z7x3k7 = new WufooForm();
				z7x3k7.initialize({
				'userName':'heroesinrecovery', 
				'formHash':'z7x3k7', 
				'autoResize':true,
				'header':'hide',
				'height':'578'});
				z7x3k7.display();
				</script>
				</div>
			</div>
			
		</div>
	
	</div>
	
</div>

<?php get_footer(); ?>