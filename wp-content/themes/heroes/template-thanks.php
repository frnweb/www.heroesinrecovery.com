<?php
/*
Template Name: [Share Your Story] Thank You
*/
?>

<?php 

if($_POST["page"]<>8) {
	//redirects to the first page if prior page was not #8, the last one in the sequence.
	header("Location: http://www.heroesinrecovery.com/share/");
	die(); //keeps everything else from executing on the page
}

get_header(); ?>

<!-- FORM HANDLERS -->
<?php 

if($_POST["form"] && $_POST["page"]==8) {
//Sends to FRN
require("./phpmailer/class.phpmailer.php");

$mail = new PHPMailer();

switch ($_POST["form"]) {
	case 'yes':
		//$mail->AddAttachment("/var/tmp/file.tar.gz");
		$name = strip_tags(stripslashes($_POST["yourname"]));
		$email = strip_tags(stripslashes($_POST["email"]));
		$avatar = $_POST["avatar"];
			if (!stristr($avatar, 'wp-content/uploads')): $mail->AddAttachment('/home/heroesin/public_html/web_content/avatars/'.$avatar); endif;
		$quote = nl2br(stripslashes(trim($_POST["quote"])));
		$quote = strip_tags($quote,"<br />,<br>");
		if($quote=="") $quote = "[nothing submitted]";
		$categories = str_replace(",", ", ", $_POST["str_categories"]);
		if($categories=="") $categories = "[none selected]";
		$video = $_POST["video"];
		$storyfull = nl2br(trim(stripslashes($_POST["storyfull"])));
		$storyfull = strip_tags($storyfull,"<br />,<br>");
		if ($storyfull == '') {
			$story1 = nl2br(stripslashes(trim($_POST["story1"])));
			$story1 = strip_tags($story1,"<br />,<br>");
			if($story1=="") $story1 = "[nothing provided]";
			$story2 = nl2br(stripslashes(trim($_POST["story2"])));
			$story2 = strip_tags($story2,"<br />,<br>");
			if($story2=="") $story2 = "[nothing provided]";
			$story3 = nl2br(stripslashes(trim($_POST["story3"])));
			$story3 = strip_tags($story3,"<br />,<br>");
			if($story3=="") $story3 = "[nothing provided]";
			$story4 = nl2br(stripslashes(trim($_POST["story4"])));
			$story4 = strip_tags($story4,"<br />,<br>");
			if($story4=="") $story4 = "[nothing provided]";
			$story5 = nl2br(stripslashes(trim($_POST["story5"])));
			$story5 = strip_tags($story5,"<br />,<br>");
			if($story5=="") $story5 = "[nothing provided]";
			$story6 = nl2br(stripslashes(trim($_POST["story6"])));
			$story6 = strip_tags($story6,"<br />,<br>");
			if($story6=="") $story6 = "[nothing provided]";
		}
		$newsletter = $_POST["newsletter"];
		if($newsletter=="") $newsletter = "Not Preferred";
		$findout = nl2br(stripslashes(trim($_POST["findout"])));
		$findout = strip_tags($findout,"<br />,<br>");
		if($findout=="") $findout = "[nothing provided]";
		
		//$intro = "<b>Name:</b> {$name}"; //Old: 's information and story submission is below:";
		break;
	case 'maybe':
		$name = strip_tags(stripslashes($_POST["yourname"]));
		$email = strip_tags(stripslashes($_POST["email"]));
		$phone = strip_tags(stripslashes($_POST["phone"]));
		$message = nl2br(stripslashes(trim($_POST["message"])));
		$message = strip_tags($message,"<br />,<br>");
		$intro = "{$name} has a few concerns or questions. Their message is below:";
		break;
	case 'no':
		$name = strip_tags(stripslashes($_POST["yourname"]));
		$email = strip_tags(stripslashes($_POST["email"]));
		$phone = strip_tags(stripslashes($_POST["phone"]));
		$message = nl2br(stripslashes(trim($_POST["message"])));
		$message = strip_tags($message,"<br />,<br>");
		$intro = "{$name} doesn't want to share their story now, but has a message below:";
		break;
}

$mail->From = "noreply@heroesinrecovery.com";
$mail->FromName = 'Heroes Website';
//The following sends everything to me if my email address is used (for testing)
if(trim($email)=="daxon.edwards@frnmail.com") $emailTo = $email;
	else $emailTo = "HeroesInRecovery@frnmail.com";
$mail->AddAddress($emailTo, "Heroes in Recovery");
$mail->IsHTML(true);

$mail->Subject = "Share Your Story Submission";
if ($POST["form"] == 'maybe' || $_POST["form"] == 'no'):
	$mail->Body    = "<i>{$intro}</i><br /><br /><b>Email:</b> {$email}<br /><b>Phone:</b> {$phone}<br /><br /><b>{$name}'s Message:</b><br />{$message}";
	$mail->AltBody = "{$intro}\r\n\r\nEmail: {$email}\r\nPhone: {$phone}\r\n\r\n{$name}'s Message:\r\n{$message}";  //removed since unnecessary for recipient to know: NOTE: You are viewing this email in plain text.\r\n\r\n
else:
	if ($storyfull == '') { $story = "<small><i>Q: Start by describing the situation that changed your life or a loved one's life.</i></small><div style=\"padding-left:15px;font-weight:bold;\"><b>A: {$story1}</b></div><br>
						  <small><i>Q: Based on your situation or story, was there a turning point that prompted the need for change or help?</i></small><div style=\"padding-left:15px;font-weight:bold;\"><b>A: {$story2}</b></div><br>
						  <small><i>Q: How did you or your HERO get help?</i></small><div style=\"padding-left:15px;font-weight:bold;\">A: {$story3}</b></div><br />
						  <small><i>Q: Based on your experience, what lessons did you learn? Do you have any advice to give?</i></small><div style=\"padding-left:15px;font-weight:bold;\"><b>A: {$story4}</b></div><br />
						  <small><i>Q: If you or your loved one is in recovery, describe what life is like today.</i></small><div style=\"padding-left:15px;font-weight:bold;\"><b>A: {$story5}</b></div><br />
						  <small><i>Q: Is there anything else you'd like to share?</i></small><div style=\"padding-left:15px;font-weight:bold;\"><b>A: {$story6}</b></div>"; 
						  
						  $story_plain = "
Q: Start by describing the situation that changed your life or a loved one's life.
A: {$story1}

Q: Based on your situation or story, was there a turning point that prompted the need for change or help?
A: {$story2}

Q: How did you or your HERO get help?
A: {$story3}

Q: Based on your experience, what lessons did you learn? Do you have any advice to give?
A: {$story4}

Q: If you or your loved one is in recovery, describe what life is like today.
A: {$story5}

Q: Is there anything else you'd like to share?
A: {$story6}";
						  } 
	else { $story = $storyfull; }
	//$story = stripslashes($story);
	//$quote = stripslashes($quote);
	if (!stristr($avatar, 'web_content/avatars')) {
		$avatar = site_url().$avatar;
		$avatar_name = str_replace(site_url()."/wp-content/uploads/", "", $avatar);
		$avatar_name = "Selected Image: ".str_replace("-150x150.jpg", "", $avatar_name);
	}
	else {
		$avatar = site_url().$avatar;
		$avatar_name = $avatar;
	}
	
	if ($video == '') { $video = "[nothing provided]"; } else { $video = "<a href='http://129.121.230.251/web_content/videos/{$video}'>Download Video</a>"; }

	
	$mail->Body    = "
					<html>
					<head>
						<style type=\"text/css\">
							table,div,p,body {font-family:Arial,Helvetica,Sans-Serif;font-size:14px;}
							a {color:#ff5800;text-underline:none;}
							a:visited {color:#ff5800;text-underline:none;}
							a:hover {color:#3e65c7;text-underline:underline;}
						</style>
					</head>
					<body>
					<font face=\"Arial, sans-serif\" size=\"3\">
					
					<center><img src='http://www.heroesinrecovery.com/wp-content/uploads/bg-link-heroes_email.jpg' alt='HEROES IN RECOVERY' height='150' /></center><br />
					<br />
					<table width='700' align='center'><tr><td>
					<div style='margin-top:25px;margin-bottom:25px;'><font size='4'>::: STORY SUBMISSION :::</font></div>
					<br /> 
					<table border='0'>
						<tr><td rowspan='5' valign='top'><div style='border:1px solid gray;margin-right:15px;margin-top:3px;margin-bottom:7px;'><a href='{$avatar}' ><img src='{$avatar}' alt='' width='150' border='0' /></a></div></td><td><font size='5'><b>{$name}</b></font></td></tr>
						<tr><td><a href='{$email}'>{$email}</a></td></tr>
						<tr><td><br />
							<small>AVATAR:</small><br />
							{$avatar_name}</td></tr>
						<tr><td><small>NEWSLETTER SIGN-UP:</small><br />
							{$newsletter}</td></tr>	
						<tr><td><small>REFERRED BY:</small><br />
							{$findout}</td></tr>
					</table>
					
					<br /> 
					<br /> 
					<p style='margin-left:3px;margin-bottom:7px;'><font size='4'><b>THE HERO'S STORY:</b></font></p>
					<table bgcolor='#f8f8f8' cellpadding='8' width='100%' style='border:1px solid #999;-moz-border-radius: 8px;border-radius: 8px;'><tr><td>
						{$story}
					</td></tr></table>
					<br /> 
					<br />
					
					<div style='margin-left:10px;'>
					<small>QUOTES:</small><br /> 
					<b>{$quote}</b><br /> <br /> 
					<small>CATEGORIES:</small><br /> 
					<b>{$categories}</b><br /> <br /> 
					<small>VIDEO:</small><br /> 
					<b>{$video}</b>
					
					</td></tr></table>
					</dov>
					<br />
					<br /> 
					<br /> 
					<br /> 
					<br /> 
					<br /> 
					<hr size='1' noshade=noshade>
					<small><span style='color:gray;'>Email sent from the <a href='http://www.heroesinrecovery.com' style='color:gray;'>Heroes in Recovery</a> website on ".date('m.d.y')." at ".date('g:i a')." from the <a href='http://www.heroesinrecovery.com/share/' style='color:gray;'>Share Your Story</a> section.<br />
					&copy; ".date('Y')." <a href='http://www.foundationsrecoverynetwork.com' style='color:gray;'>Foundations Recovery Network</a></span></small>
					
					</font></body></html>";
	$mail->AltBody = "HEROES IN RECOVERY STORY SUBMISSION

	
{$name}
{$email}

Newsletter Sign-up: {$newsletter}
Referred by:        {$findout}
Avatar URL:         {$avatar}


THE HERO'S STORY:
{$story_plain}



QUOTES: 
{$quote}

CATEGORIES:
{$categories}

VIDEO:
{$video}





- - - -
Email sent from the Heroes in Recovery website on ".date('m.d.y')." at ".date('g:i a')." from the Share Your Story (http://www.heroesinrecovery.com/share/) section.
© ".date('Y')." Foundations Recovery Network (http://www.foundationsrecoverynetwork.com)


";
endif;



///////
//Sends to Storyteller
///////
//if(trim($email)=="daxon.edwards@frnmail.com") {
	$mail_copy = new PHPMailer();
	$mail_copy->From = "noreply@heroesinrecovery.com";
	$mail_copy->FromName = 'Heroes in Recovery';
	$mail_copy->AddAddress("$email", $name);
	$mail_copy->IsHTML(true);
	$mail_copy->Subject = "Thanks for sharing your story!";
	$mail_copy->Body = '
	<html>
	<body>
	<TABLE cellSpacing="0" cellpadding="0" bgcolor="#ffffff" border=0 align="center" width="499">

    <TR>
		<TD bgcolor="#FFFFFF" width="499" align="center" valign="top">
			<img src="http://www.aboutrecovery.com/newsletter/whiteborder.gif" width="45" height="15" hspace="0" vspace="0" border="0" align="left"></td>
    </tr>
   	<tr>
        <td valign="top"><div align="center"><a href="http://heroesinrecovery.com/"><img src="http://www.heroesinrecovery.com/wp-content/uploads/thank-you-heroes.gif" alt="" hspace="0" vspace="0" border="0" /></a></div></td>
    </tr>
    <tr>
    	<td valign="top"><div align="left" style="font-size:13px;"><FONT face="Georgia, Times New Roman, Times, serif" color="#333333">
			Thank you for submitting your story to <a href="http://www.heroesinrecovery.com" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes in Recovery</strong></a>. Your story may  be the one that encourages someone to seek the addiction and mental health help they need without feeling ashamed or isolated. Together, we are breaking the stigma one story at a time!<br />
			<br />
    		Before your story can be posted to the website, the Heroes team will need to review and make any edits if necessary.  We have  a set of rules we use to protect everyone\'s privacy, maintain the values of Heroes in Recovery and aid the reading experience. Please read our <a href="http://www.heroesinrecovery.com/wp-content/uploads/HeroesStyleGuide.pdf" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>guidelines</strong></a> to find out how or why your story may have been edited.<br /><br />
			<em>In the meantime, follow us on social media and join the conversation!</em></font>
            <div align="center"><img src="http://www.heroesinrecovery.com/wp-content/uploads/social-media-icons.gif" alt="social media icons" width="270" height="88" usemap="#Map" hspace="0" vspace="0" border="0" /></div>
			</div></td>
	</tr>
	<tr>
          <td bgcolor="#ffffff" width="499" align="center"><br />
		    <div align="center" style="font-size:12px;">
			<font face="arial" color="#333333"><strong>Heroes in Recovery</strong><br />5409 Maryland Way | Suite 320 | Brentwood, TN 37027 <br /><a href="http://www.facebook.com/HeroesinRecovery" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Facebook</strong></a> | <a href="http://twitter.com/heroesnrecovery" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Twitter</strong></a> | <a href="http://www.pinterest.com/heroesnrecovery/" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Pinterest</strong></a> | <a href="http://instagram.com/heroesinrecovery#" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Instagram</strong></a><br>
            <br> </font>
			</div></td>
     </tr>
	</table>

	<map name="Map" id="Map">
	  <area shape="rect" coords="8,12,69,75" href="http://twitter.com/heroesnrecovery" />
	  <area shape="rect" coords="73,15,133,75" href="http://www.pinterest.com/heroesnrecovery/" />
	  <area shape="rect" coords="138,14,199,77" href="http://www.facebook.com/HeroesinRecovery" />
	  <area shape="rect" coords="202,14,264,79" href="http://instagram.com/heroesinrecovery#" />
	</map>
	</body>
	</html>
	';
	$mail_copy->AltBody = "
Thank you for submitting your story to Heroes in Recovery! Your story may be the one that encourages someone to seek the addiction and mental health help they need without feeling ashamed or isolated. Together, we are breaking the stigma one story at a time!

Before your story can be posted to the website, the Heroes team will need to review and make any edits if necessary. We have a set of rules we use to protect everyone's privacy, maintain the values of Heroes in Recovery and aid the reading experience. Please read our guidelines (http://www.heroesinrecovery.com/wp-content/uploads/HeroesStyleGuide.pdf) to find out how or why your story may have been edited.

In the meantime, follow us on social media and join the conversation if you haven't already!"."

http://twitter.com/heroesnrecovery
http://www.pinterest.com/heroesnrecovery/
http://www.facebook.com/HeroesinRecovery
http://instagram.com/heroesinrecovery#


Heroes in Recovery (www.heroesinrecovery.com)
5409 Maryland Way | Suite 320 | Brentwood, TN 37027

	"; //plain text
	$mail_copy->Send();
//}

/*
$mail->IsSMTP();                                      // set mailer to use SMTP
$mail->Host = "smtp1.example.com;smtp2.example.com";  // specify main and backup server
$mail->SMTPAuth = true;     // turn on SMTP authentication
$mail->Username = "jswan";  // SMTP username
$mail->Password = "secret"; // SMTP password

$mail->AddReplyTo("info@example.com", "Information");
$mail->WordWrap = 150;                                 // set word wrap to 50 characters
*/
} //Ends requirement that the previous page be #8 (the last page in the sequence)
?>

<div id="main">

	<div id="thanks">
	
		<?php
		if(!$mail->Send())
		{
		   echo "<p>Message could not be sent. Please refresh this page and click to resend your information again. If you continue to get this error, please <a href='http://www.heroesinrecovery.com/help/'>contact us</a> and send the information to us. We'll be glad to submit the information for you!</p>";
		   echo "<p>Here is the techie error code to give us too: " . $mail->ErrorInfo . "</p>";
		   exit;
		}
		?>
		
		<h1><span class="one">Thanks</span> <span class="two">for</span> <span class="three">sharing</span></h1>
		<h2>Keep an eye on your email. Your story will be posted soon. If you would ever like to have your story taken down, <a href="/help">contact us</a>.</h2>
		<div class="now-what">
			<div class="left">
				<h3>You are now officially one of our <span>heroes!</span></h3>
				<p>Now that you have shared your story, you may be asking what else can one person do to help break the stigma of addiction and mental heath disorders. We are so glad you asked, because we think that you can do quite a bit. Here are a few suggestions.</p>
			</div>
			<img src="/wp-content/themes/heroes/images/thankyou-hrarrow.png" />
		</div>
		
		<div class="tip clearfix">
			<div class="encourage">
				<p>
				If you shared your story here on HeroesinRecovery.com or are just clicking around on our site, then chances are you know some 
				other people who have an inspiring journey of recovery that they could share as well. Or maybe you know someone who is 
				struggling with an addiction or mental health disorder who would benefit from the inspiring stories here. Spread the word 
				about Heroes in Recovery by word of mouth, over email or on social media. 
				</p>
			</div>
		</div>
		
		<!--
		<div class="tip clearfix">
			<div class="tshirts">
				<img src="/wp-content/themes/heroes/images/thankyou-tshirts-img.jpg" />
				<p>
				Use these handy Do-It-Yourself stencils from our resource page to make your own shirts. Wear them to spark curiosity. Your friends 
				and the people you meet are likely to ask you what your shirts all about, giving you the opportunity to spread the word about Heroes 
				In Recovery
				</p>
			</div>
		</div>
		-->
		
		<div class="tip clearfix">
			<div class="sixk">
				<img src="/wp-content/themes/heroes/images/thankyou-6k-img.jpg" />
				<p>
				Our <a href="http://www.heroesinrecovery6k.com" target="_blank">Heroes 6Ks</a> are a great way to get involved, and one just might be coming to a location near you. If so, grab your friends, 
				family and anyone you know in the recovery community and come to the race! 
				</p>
			</div>
		</div>
		
		<!--
		<div class="tip clearfix">
			<div class="stickerup">
				<img src="/wp-content/themes/heroes/images/thankyou-stickerup-img.jpg" />
				<p>
				Here, there and everywhere. That's where you can stick these cool Heroes In Recovery stickers. You'll just need some sticker label 
				sheets and a color printer. Visit the resource page to download.  
				</p>
			</div>
		</div>
		-->

	</div>
	
</div>

<?php get_footer(); ?>
