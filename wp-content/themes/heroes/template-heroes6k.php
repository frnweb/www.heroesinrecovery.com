<?php 
/* Template Name: Heroes 6k */
get_header(); 
?>

<div id="main" class="heroes6k">
	<?php while (have_posts()) : the_post(); ?>
	<div id="content6k">
		<h1><?php the_title(); ?></h1>
		<?php 
			// check if the post has a Post Thumbnail assigned to it.
			if ( has_post_thumbnail() ) {
		?>
		<div id="headerimg">
				<?php the_post_thumbnail('full'); ?>
		</div>
		<?php 
		} 
		?>
		<?php the_content(); ?>
	</div>
	<?php endwhile; ?>
	
	<div id="sidebar6k">
		<div class="clearfix" id="socialContainer">
			<div class="social-media">
			<a href="https://www.facebook.com/HeroesinRecovery">
			<img src="/wp-content/themes/heroes/images/icon-facebook.png" />
			</a>
			<a href="http://twitter.com/#!/HeroesNRecovery">
			<img src="/wp-content/themes/heroes/images/icon-twitter.png" />
			</a>
			<a href="http://www.youtube.com/user/foundationsrnetwork">
			<img src="/wp-content/themes/heroes/images/icon-youtube.png" />
			</a>
			<a href="/feed/">
			<img src="/wp-content/themes/heroes/images/icon-rss.png" />
			</a>
			</div>
		</div>
		<div id="raceLocationsContainer">
			<div align="center"><h2>Race Locations</h2></div>
			<ul>
			<?php 
			$pages = get_pages('title_li=&child_of=2216&echo=0');
			foreach($pages as $page)
			{ ?>
				<li id="pages"><a href="<?php echo get_page_link($page->ID) ?>"><?php echo get_post_meta($page->ID,'hir_6k_race_city', true);?>, <?php echo get_post_meta($page->ID,'hir_6k_race_state', true);?></a></li>
			<?php } ?>
			</ul>
		</div>
		<?php if ( function_exists('dynamic_sidebar') ) { dynamic_sidebar('Races Sidebar'); } ?>
	</div>
	
	<div style="clear:both;"></div>

</div>

<?php get_footer(); ?>