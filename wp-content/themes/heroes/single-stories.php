<?php get_header();  ?>

<div id="main" class="story">
	<?php 
 	$value = get_post_meta($post->ID, 'hir_logo_letter', true);
 	
 		if($value == 'value1') {
 			$islogo = true;
 			$hir_letter = '';
 		} elseif($value == 'value2') {
 			$hir_letter = 'h';
 			$hir_letter_uppercase = 'H';
 			$strltr = 'h';
 		} elseif($value == 'value3') {
 			$hir_letter = 'e';
 			$hir_letter_uppercase = 'E';
 			$strltr = 'e';
 		} elseif($value == 'value4') {
 			$hir_letter = 'r';
 			$hir_letter_uppercase = 'R';
 			$strltr = 'r';
 		} elseif($value == 'value5') {
 			$hir_letter = 'o';
 			$hir_letter_uppercase = 'O';
 			$strltr = 'o';
 		} elseif($value == 'value6') {
 			$hir_letter = 'ee';
 			$hir_letter_uppercase = 'E';
 			$hir_letter_uppercase = 'E';
 			$strltr = 'e';
 		} elseif($value == 'value7') {
 			$hir_letter = 's';
 			$hir_letter_uppercase = 'S';
 			$strltr = 's';
 		} else {
 			
 		}
 	?>
	
	
	 <?php $islogo = false; if($value != 'value1'): $islogo = true; /* $letter = $cfs->get('logo_letter', $post->ID); if ($letter['None'] == ''): $islogo = true;  $strltr = implode('', $letter); $ltr = strtolower($strltr);*/ ?> 
			
 	
 	
	
	<div id="logo-story">
		<div class="gallery">
			<div class="gallery-holder">
				<ul>
					<li>
						<div class="box logo-left-<?=$hir_letter?>">
							<p class="tagline1"><?php echo get_post_meta($post->ID,'hir_hero_left_box_tagline', true);?></p>
							<p class="tagline2"><?php echo get_post_meta($post->ID,'hir_hero_right_box_tagline', true);?></p>
						</div>
					</li>
					<li>						
						<div class="box" style="padding:0; width:294px; height:284px;">
							<?php echo the_post_thumbnail( 'full' ); ?>
						</div>
					</li>
					<li>					
						<div class="box logo-right-<?=$hir_letter?>">
							<p class="title"><span class="name"><?php echo get_post_meta($post->ID,'hir_hero_story_name', true);?>'s</span><span class="story">Story</span></p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div id="logo-sidebar">
		<div class="hero-logo">
			<img class="img-<?=$hir_letter?>" alt="Heroes' Letter" src="/wp-content/themes/heroes/images/img-<?=$hir_letter?>.jpg" width="190" height="184">
			<p><?php echo get_post_meta($post->ID,'hir_hero_story_name', true);?>'s story is captured in this letter "<?=$hir_letter_uppercase?>" from the HEROES logo</p>
		</div>
	</div>
	
	<div id="stories" style="border:none;">

		<div class="story story-letter">
			<h2><?php the_title(); ?></h2>
			<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
			<?php while (have_posts()) { the_post(); the_content(); } ?>
			<div class="comments">
				<span class="number"><?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?> <strong>Tags:</strong></span>
				<?php the_tags('<ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
			</div>
			<div class="social-box" style="display:none;">
				<ul class="social-plagin">
					<li><a href="<?php the_permalink()?>#respond"><img src="<?php bloginfo('template_url'); ?>/images/btn-comment.gif" width="98" height="23" alt="image description" /></a></li>
					<li><span class="st_facebook_custom"><img src="<?php bloginfo('template_url'); ?>/images/btn-f-share.gif" width="61" height="23" alt="image description" /></span></li>
					<li><iframe src="http://www.facebook.com/plugins/like.php?app_id=251273934887780&amp;href&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe></li>
					<li><span class="st_twitter_custom"><img src="<?php bloginfo('template_url'); ?>/images/btn-t-share.gif" width="62" height="23" alt="image description" /></span></li>
					<li><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
				</ul>
				<div class="rating-box">
					<span>Rate:</span>
				</div>
				<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
			</div>
			<?php comments_template(); ?>
		</div>	
		
		<div class="navigation">
			<div class="prev"><?php next_post_link('&laquo; Previous: %link') ?></div>
			<div>&nbsp;&nbsp; | &nbsp;&nbsp;</div>
			<div class="next"><?php previous_post_link('Next: %link &raquo;') ?></div>
		</div>	
	</div>
	
	<?php else: ?>

	<div id="stories" class="story-detail">
			
		<section class="clearfix">
		
			<?php while (have_posts()) : the_post(); $terms = wp_get_post_terms( $post->ID, 'story_category' ); $cat = $terms[0]->name; $catcolor = $terms[0]->description; $catslug = $terms[0]->slug; ?>
			
			<?php
			// Get a comma-separated list of categories
			foreach ($terms as $term):
				$termlist .= '<a href="/stories/?c='.$term->slug.'" rel="nofollow">'. $term->name .'</a>' . ', ';
			endforeach;
			$termlist = rtrim($termlist, ', ');
			?>
			
			<aside>
				<div id="thumbnail">
				<?php if ( has_post_thumbnail() ): the_post_thumbnail( array(100,100) ); endif; ?>
				</div>
				<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { 
				echo '
				<div style="margin-top:40px; text-align: center;">';
					ADDTOANY_SHARE_SAVE_KIT(); 
					echo '
				</div>
				';
				} ?>
			</aside>

			<article>
				<h1 style="color:<?php echo $catcolor; ?>;">
					<?php echo get_post_meta($post->ID,'hir_hero_story_name', true);?>
					<span><?php the_title(); ?></span>
				</h1>
				<div class="meta">
					Posted on <span><?php the_date(); ?></span><br />
					Tagged in <span><?php echo $termlist; ?></span><br />
					<?php if(get_post_meta($post->ID,'hir_hero_submitted_by', true)) { ?>
						Submitted By <?php echo get_post_meta($post->ID,'hir_hero_submitted_by', true);?><br /><br />			
					<?php } else {  ?>
						<br />
					<?php } ?>
				</div>
				<?php the_content(); ?>
				
				<?php comments_template(); ?>
			</article>
			
			<? endwhile; ?>
			
			<div class="clear:left;"></div>
			
		</section>
		
		
		<section class="clearfix">
		
			<div id="related-stories">
				<h3>Other stories related to <?php echo $cat; ?></h3>
			</div>
			
			<?php
			wp_reset_query();
			$args = array('post_type' => 'stories', 'orderby' => 'rand', 'posts_per_page' => 5, 'tax_query' => array( array('taxonomy' => 'story_category', 'field' => 'slug', 'terms' => ''. $catslug .'', 'operator' => 'IN') ));
			query_posts($args);
			?>
			
			<?php while (have_posts()) : the_post(); $terms = wp_get_post_terms( $post->ID, 'story_category' ); ?>
			
			<aside>
				<div id="thumbnail">
				<?php if ( has_post_thumbnail() ): the_post_thumbnail( array(100,100) ); endif; ?>
				</div>
			</aside>

			<article>
				<a href="<?php the_permalink(); ?>">
				<h1 style="color:<?php echo $catcolor ?>;">
					<?php echo get_post_meta($post->ID,'hir_hero_story_name', true);?>
					<span><?php the_title(); ?></span>
				</h1>
				</a>
				<?php the_excerpt(); ?>
				
				<?php comments_template(); ?>
			</article>
			
			<div class="clear:left;"></div>
			
			<? endwhile; ?>
			
		</section>
	
	</div>
	
	<div id="story-sidebar">
		<ul class="story-categories">
			<li style="background-color:#918c8e;"><a rel="all" href="http://www.heroesinrecovery.com/stories/">Reset to<br /><span>All Stories</span></li></a>
		<?php 
		$terms = get_terms( 'story_category', array('hide_empty' => false, 'orderby' => 'id') );
		foreach ( $terms as $term ): if ($cat==$term->name) { $active=' class="active"'; } else { $active=''; } ?>
			<li style="background-color:<?php echo $term->description ?>;"><a rel="nofollow" href="/stories/?c=<?php echo $term->slug ?>">Heroic stories relating to<br /><span><?php echo $term->name ?></span></a></li>
		<?php endforeach; ?>
		</ul>
	</div>
	
	<div style="clear:both;"></div>

	<?php endif; ?>	

</div>

<?php get_footer(); ?>