<?php get_header(); ?>
<div id="main">
	<div id="events">
	
		<h1>News &amp; Events</h1>
		<?php global $query_string; ?>
		<?php query_posts($query_string . '&paged='.$paged);?>
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<article class="event clearfix">
				
				<div class="thumb">
					<?php 
					if (has_post_thumbnail()): 
						the_post_thumbnail( array(250,250) ); 
					else: 
						echo '<img class="attachment-250x250 wp-post-image" width="250" height="250" alt="heroes-thumb" src="/wp-content/uploads/heroes-thumb.png">';
					endif; 
					?>
				</div>
				
				<div class="content">
					<?php
					$terms = wp_get_post_terms( get_the_ID(), 'event_category');
					?>
					<h1><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h1>
					<?php if(get_post_meta($post->ID,'hir_event_day', true)) { ?>
						<p class="meta">Date of Event: <span><?php echo get_post_meta($post->ID,'hir_event_day', true);?></span></p>
					<?php }  ?>
					<p class="meta">News Type: <span><?php echo $terms[0]->name; ?></span></p>
					<p class="excerpt"><?php echo get_post_meta($post->ID,'hir_event_excerpt', true);?></p>
					<p><a href="<?php the_permalink(); ?>">Read more ></a></p>
				</div>
				
			</article>
		<?php endwhile; ?>
		<div class="navigation ev">
			<?php ranklab_pagination();?>
		</div>
		<?php else: ?>
			<h3>Not Found</h3>
			<p>Sorry, but you are looking for something that isn't here.</p>
		<?php endif; ?>
	</div>
	
	<div id="event-sidebar">
		<?php get_sidebar('1'); ?>
	</div>
</div>
<?php get_footer(); ?>
