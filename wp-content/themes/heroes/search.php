<?php get_header(); $debug = $_GET["debug"] == 'true' ? true : false; ?>

<div id="main" class="story">

	<h1>Heroic Stories. Real Journeys.</h1>
	<h2>Real recovery begins with real people. Be inspired by these courageous stories of recovery, life, and hope. Share your own for someone else.</h2>

	<div id="stories">
		
		<div id="story-info">
			<?php if ($debug): ?>
			<div class="sort"> 
				<?php get_search_form(); ?>
			</div>
			<?php endif; ?>
			<p>
			<?php 
			if ( is_search() ) {
				echo 'Found ' . $wp_query->found_posts . ' stories';
			}
			else
				echo ucwords( str_replace('-', ' ', $category) ); 
			?>
			</p>
		</div>
		
		
		<? /*
		<?php $p=0; while (have_posts()) : the_post(); $p++; ?>
		
			<?php if ($p==5): //Center box ?>
	
				<div class="story-box promo">
					<a href="/share/">
						<div class="story-photo"><img src="/wp-content/themes/heroes/images/story-share.png" /></div>
						<div class="story-hero" style="background-color:#5c7f9f; font-family:Georgia, 'Times New Roman', serif; color:#FFFFFF; text-align:center; font-style:italic;"><p style="padding:17px 0 0 0; margin:0; font-size:16px;">Be an Inspiration ></p></div>
					</a>
				</div>
				
			<?php else: ?>
			
				<div class="story-box">

					<a href="<?php the_permalink(); ?>">
					<?php $terms = wp_get_post_terms( $post->ID, 'story_category' ); ?>
					<div class="story-photo">
						<?php if ( has_post_thumbnail() ) : the_post_thumbnail(array(235,235)); endif; ?>
					</div>
					<div class="story-hero" style="background-color:<?php echo $terms[0]->description; ?>;">
						<p><span><?php echo $cfs->get('hero_name', $post->ID); ?></p>
					</div>
					</a>
				</div>
			
			<?php endif; ?>
		
		<?php endwhile; ?>
		
		<div id="story-nav">
			<?php if(function_exists('wp_paginate')) {
				wp_paginate();
			} ?>
		</div>
		
		*/ ?>
		
		<?php
		global $wpdb;
	    // If you use a custom search form
	    // $keyword = sanitize_text_field( $_POST['keyword'] );
	    // If you use default WordPress search form
	    $keyword = get_search_query();
	    $keyword = '%' . like_escape( $keyword ) . '%'; // Thanks Manny Fleurmond
	    // Search in all custom fields
	    $post_ids_meta = $wpdb->get_col( $wpdb->prepare( "
	    SELECT DISTINCT post_id FROM {$wpdb->postmeta}
	    WHERE meta_value LIKE '%s'
	    ", $keyword ) );
	    // Search in post_title and post_content
	    $post_ids_post = $wpdb->get_col( $wpdb->prepare( "
	    SELECT DISTINCT ID FROM {$wpdb->posts}
	    WHERE post_title LIKE '%s'
	    OR post_content LIKE '%s'
	    ", $keyword, $keyword ) );
	    $post_ids = array_merge( $post_ids_meta, $post_ids_post );
	    // Query arguments
	    $args = array(
	    'post_type' => 'stories',
	    'post_status' => 'publish',
	    'post__in' => $post_ids,
	    'posts_per_page' => '200',
	    'meta_key' => '_thumbnail_id',
	    '&paged=' => $paged
	    );
	    $wp_query = new WP_Query( $args );
	    if ( $wp_query->have_posts() ): while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
	    
	    	<?php if ($p==5): //Center box ?>
	
				<div class="story-box promo">
					<a href="/share/">
						<div class="story-photo"><img src="/wp-content/themes/heroes/images/story-share.png" /></div>
						<div class="story-hero" style="background-color:#5c7f9f; font-family:Georgia, 'Times New Roman', serif; color:#FFFFFF; text-align:center; font-style:italic;"><p style="padding:17px 0 0 0; margin:0; font-size:16px;">Be an Inspiration ></p></div>
					</a>
				</div>
				
			<?php else: ?>
			
				<div class="story-box">
					<a href="<?php the_permalink(); ?>">
					<?php $terms = wp_get_post_terms( $post->ID, 'story_category' ); ?>
					<div class="story-photo">
						<?php if ( has_post_thumbnail() ) : the_post_thumbnail(array(235,235)); endif; ?>
					</div>
					<div class="story-hero" style="background-color:<?php echo $terms[0]->description; ?>;">
						<p><span><?php echo get_post_meta($post->ID,'hir_hero_story_name', true);?></p>
					</div>
					</a>
				</div>
			
			<?php endif; ?>
	    
	    <?php endwhile;  ?>
	    
	    <div id="story-nav">
			<?php ranklab_pagination();?>
		</div>
		<?php endif; ?>
	</div>
	
	<div id="story-sidebar">
		<ul class="story-categories">
			<!--<li class="main" style="background-color:#918c8e;"><a onclick="return false;" href="" rel="none"><span>EXPLORE</span><br />Click categories<br /> below to filter</a></li>-->
			<li style="background-color:#b69a71;"><a href="/stories/">Reset to<br /><span>All Stories</span></a></li>
		<?php 
		$terms = get_terms( 'story_category', array('hide_empty' => false, 'orderby' => 'id') );
		foreach ( $terms as $term ): ?>
			<li style="background-color:<?php echo $term->description ?>;"><a rel="nofollow" href="/stories/?c=<?php echo $term->slug ?>">Heroic stories relating to<br /><span><?php echo $term->name ?></span></a></li>
		<?php endforeach; ?>
		</ul>
	</div>
	
	<div style="clear:both;"></div>

</div>

<?php get_footer(); ?>