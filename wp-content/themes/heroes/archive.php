<?php get_header(); ?>

<div id="main">

	<div class="twocolumns twocolumns-2">
		<div id="content">
			<div class="two-cols">
				<div class="post-col">
				<?php get_template(); ?>
				<?php if (have_posts()) : ?>
					<div class="title-holder">
						<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
						<?php /* If this is a category archive */ if (is_category()) { ?>
						<h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>
						<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
						<h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
						<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
						<h2>Archive for <?php the_time('F jS, Y'); ?></h2>
						<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
						<h2>Archive for <?php the_time('F, Y'); ?></h2>
						<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
						<h2>Archive for <?php the_time('Y'); ?></h2>
						<?php /* If this is an author archive */ } elseif (is_author()) { ?>
						<h2>Author Archive</h2>
						<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
						<h2>Blog Archives</h2>
						<?php } ?>
					</div>
					<?php while (have_posts()) : the_post(); ?>
						<div class="post">
							<div class="author-box">
								<?php $user_info = get_userdata($post->post_author); ?>
								<a href="<?php echo $user_info-> user_url; ?>" class="img-box"><?php echo get_avatar($post->post_author, 102); ?></a>
								
								<div class="about">
									<p align="center"><a href="<?php echo $user_info-> user_url; ?>"><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></a> <?php echo $user_info->user_description; ?></p>
									<?php if(!is_author()) { ?><a href="<?php echo $user_info-> user_url; ?>" class="link-more">More About <?php echo $user_info->user_firstname; ?> &gt;</a><?php } ?>
									<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { 
										echo '
									<div style="margin-top:40px; text-align: center; line-height:300%;">';
										ADDTOANY_SHARE_SAVE_KIT(); 
										echo '
									</div>
									';
									} ?>
								</div>
							</div>
							<div class="post-content">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
								<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></strong> | currently <?php comments_popup_link('No commenting', '1 commenting', '% commenting'); ?></span>
								<?php the_content(); ?>
								<div class="comments">
									<span class="number"><?php //comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?> <strong>Tags:</strong></span>
									<?php the_tags('<ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
								</div>
								<div class="social-box" style="display:none;">
									<ul class="social-plagin">
										<li><a href="<?php the_permalink()?>#respond"><img src="<?php bloginfo('template_url'); ?>/images/btn-comment.gif" width="98" height="23" alt="image description" /></a></li>
										<li><span class="st_facebook_custom"><img src="<?php bloginfo('template_url'); ?>/images/btn-f-share.gif" width="61" height="23" alt="image description" /></span></li>
										<li><iframe src="http://www.facebook.com/plugins/like.php?app_id=251273934887780&amp;href&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe></li>
										<li><span class="st_twitter_custom"><img src="<?php bloginfo('template_url'); ?>/images/btn-t-share.gif" width="62" height="23" alt="image description" /></span></li>
										<li><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
									</ul>
									<div class="rating-box">
										<span>Rate:</span>
									</div>
									<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					<div class="navigation">
						<div class="next"><?php next_posts_link('Older Entries &raquo;') ?></div>
						<div class="prev"><?php previous_posts_link('&laquo; Newer Entries') ?></div>
					</div>
				<?php else: ?>
					<h2>Not Found</h2>
					<p>Sorry, but you are looking for something that isn't here.</p>
				<?php endif; ?>
				</div>
			</div>
		</div>
		<?php get_sidebar('3'); ?>
	</div>

	
</div>

<?php get_footer(); ?>