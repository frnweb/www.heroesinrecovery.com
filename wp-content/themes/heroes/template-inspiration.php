<?php 
/*
Template Name: Logo Inspiration
*/
get_header(); ?>

<div id="main">
	<div id="page">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

				<div class="form-block">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<table class="logos">
					<tbody>
					<tr>
						<?php 
						$logo_stories = $wpdb->get_results( $wpdb->prepare( "SELECT cfs.post_id, pm.meta_value FROM wp_cfs_values cfs INNER JOIN wp_postmeta pm ON cfs.meta_id = pm.meta_id WHERE cfs.field_id = 11 AND pm.meta_value <> '' ORDER BY pm.meta_id DESC;" ) );
						usort($logo_stories, "sortary");
						?>
						<? foreach ($logo_stories as $letter): ?>
						<td><a href="<?=get_permalink($letter->post_id);?>"><img class="alignleft size-full wp-image-296" src="/wp-content/themes/heroes/images/img-thm-<?=strtolower($letter->meta_value);?>.png" alt="" width="85" height="85" /></a></td>
						<? endforeach; ?>
					</tr>
					</tbody>
					</table>
				</div>

			<?php endwhile; ?>
		<?php else: ?>
			<h2>Not Found</h2>
			<p>Sorry, but you are looking for something that isn't here.</p>
		<?php endif; ?>
	</div>
	
	<div id="page-sidebar">
		<?php get_sidebar('1'); ?>
	</div>
</div>

<?php get_footer(); ?>
