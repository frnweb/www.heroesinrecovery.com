<?php get_header(); ?>

<div id="main">
	<div id="page">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

				<div class="form-block">

<style type="text/css" media="screen">
  .title_shares {
    margin-top:15px;
    margin-left:15px;
    float:left;
  }
  .title_shares div {
    float:left;
  }
  #custom-tweet-button {
    display:inline;
  }
  #custom-tweet-button a {
    padding: 2px 5px 2px 20px;
    font-family:Arial,Sans-serif;
    font-size:10px;
    font-weight:none;
    text-transform:none;
    color:#5A98A5;
  }
  #custom-tweet-button a img {
    height:25px;
    vertical-align:middle;
  }
</style>


					<h1 style="float:left;margin-right:15px;display:none;"><?php the_title(); ?></h1>
						<!--<div class="title_shares">
							<div class="fb-share-button" data-href="<?php the_permalink()?>"></div>
							<div id="custom-tweet-button">						  
								<?php echo do_shortcode('[frn_social version="text" account="HeroesNRecovery" related="FRNetwork" related_tagline="Creating lifetime relationships for long-term recovery." link_text="http://www.heroesinrecovery.com/wp-content/plugins/acurax-social-media-widget/images/themes/14/twitter.png"]'); ?>
							</div>
						</div>-->
						<div style="clear:both;"></div>

					<?php the_content(); ?>
				</div>

			<?php endwhile; ?>
		<?php else: ?>
			<h2>Not Found</h2>
			<p>Sorry, but you are looking for something that isn't here.</p>
		<?php endif; ?>
	</div>
	
	<div id="page-sidebar">
		<?php get_sidebar('1'); ?>
	</div>
</div>

<?php get_footer(); ?>