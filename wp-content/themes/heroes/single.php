<?php get_header(); ?>

<div id="main">
	<div class="twocolumns twocolumns-2">
		<div id="content">
			<div class="two-cols">
				<div class="post-col">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<div class="post">
							<div class="author-box">
								<?php $user_info = get_userdata($post->post_author); ?>
								<a href="<?=get_author_posts_url($post->post_author); ?>" class="img-box"><?php echo get_avatar($post->post_author, 102, '', $user_info->user_firstname . ' ' . $user_info->user_lastname); ?></a>
								
								<div class="about">
									<p align="center"><a href="<?=get_author_posts_url($post->post_author); ?>"><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></a><br /><br /> <?php echo $user_info->user_description; ?></p>
								</div>
								<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { 
								echo '
								<div style="margin-top: 5px; text-align: center;line-height: 300%;">';
									ADDTOANY_SHARE_SAVE_KIT(); 
									echo '
								</div>
								';
								} ?>
								<!--<div class="fb-share-button" data-href="<?php $permalink = get_permalink(); ?>" data-type="icon_link"></div>-->
							</div>
							<div class="post-content">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
								<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></strong><!-- | currently --><?php //comments_popup_link('No commenting', '1 commenting', '% commenting'); ?></span>
								<?php the_content(); ?>
								<div class="comments">
									<span class="number"><?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?> <strong>Tags:</strong></span>
									<?php the_tags('<ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
								</div>
								<div class="social-box" style="display:none;">
									<ul class="social-plagin">
										<li><div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div><!--<span class="st_facebook_custom"><img src="<?php bloginfo('template_url'); ?>/images/btn-f-share.gif" width="61" height="23" alt="Share this story on your Facebook" /></span>--></li>
										<li></li>
										<li><span class="st_twitter_custom"><img src="<?php bloginfo('template_url'); ?>/images/btn-t-share.gif" width="62" height="23" alt="image description" /></span></li>
										<li><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
									</ul>
									<div class="rating-box">
										<span>Rate:</span>
									</div>
									<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
									<div style="display:inline;"><a href="<?php the_permalink()?>#respond"><img src="<?php bloginfo('template_url'); ?>/images/btn-comment.gif" width="98" height="23" alt="image description" /></a></div>
								</div>
							<?php comments_template(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<div class="navigation">
						<div class="next"><?php previous_post_link('%link &raquo;') ?></div>
						<div class="prev"><?php next_post_link('&laquo; %link') ?></div>
					</div>
				<?php else: ?>
					<h2>Not Found</h2>
					<p>Sorry, but you are looking for something that isn't here.</p>
				<?php endif; ?>
				</div>
			</div>
		</div>
		<?php get_sidebar('3'); ?>
	</div>
</div>

<?php get_footer(); ?>