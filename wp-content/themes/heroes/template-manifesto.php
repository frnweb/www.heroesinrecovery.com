<?php
/*
Template Name: Manifesto Template
*/
?>

<?php get_header(); ?>

<div id="main">

	<div class="twocolumns">
		<div id="content" class="manifesto">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<div class="c1">
				<div class="text-block">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<?php endwhile; ?>
			<?php else: ?>
				<h2>Not Found</h2>
				<p>Sorry, but you are looking for something that isn't here.</p>
			<?php endif; ?>
		</div>
		<?php get_sidebar('1'); ?>
	</div>
</div>

<?php get_footer(); ?>
