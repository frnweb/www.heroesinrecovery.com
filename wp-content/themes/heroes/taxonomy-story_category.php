<?php 
get_header(); 
if ($_POST):
	$order = $_POST["sort"];
else:
	$order = 'date';
endif;
?>

<div id="main" class="story">

	<h1>Heroic Stories. Real Journeys.</h1>
	<h2>Real recovery begins with real people. Be inspired by these courageous stories of recovery, life, and hope. Share your own for someone else.</h2>

	<div id="stories">
	
		<div id="story-info">
			<div class="sort"> 
				<?php get_search_form(); ?>
			</div>
			<p><?php echo ucwords( str_replace('-', ' ', $category) ); ?></p>
		</div>

	
		<?php $p=0; while (have_posts()) : the_post(); $p++; ?>
		
			
			
				<div class="story-box">

					<a href="<?php the_permalink(); ?>">
					<?php $terms = wp_get_post_terms( $post->ID, 'story_category' ); ?>
					<div class="story-photo">
						<?php if ( has_post_thumbnail() ) : the_post_thumbnail(array(235,235)); endif; ?>
					</div>
					<div class="story-hero" style="background-color:<?php echo $terms[0]->description; ?>;">
						<p><span><?php echo get_post_meta($post->ID,'hir_hero_story_name', true);?></p>
					</div>
					</a>
				</div>
			
			
		
		<?php endwhile; ?>
		
		<div id="story-nav">
			<?php ranklab_pagination();?>
		</div>

	</div>
	
	<div id="story-sidebar">
		<ul class="story-categories">
			<!--<li class="main" style="background-color:#918c8e;"><a onclick="return false;" href="" rel="none"><span>EXPLORE</span><br />Click categories<br /> below to filter</a></li>-->
			<li style="background-color:#b69a71;"><a href="/stories/">Reset to<br /><span>All Stories</span></a></li>
		<?php 
		$terms = get_terms( 'story_category', array('hide_empty' => false, 'orderby' => 'id') );
		foreach ( $terms as $term ): ?>
		<?php  $term_link = get_term_link( $term ); ?>
			<li style="background-color:<?php echo $term->description ?>;"><a rel="nofollow" href="<?php echo esc_url( $term_link ); ?>">Heroic stories relating to<br /><span><?php echo $term->name ?></span></a></li>
		<?php endforeach; ?>
		</ul>
	</div>
	
	<div style="clear:both;"></div>

</div>

<?php get_footer(); ?>