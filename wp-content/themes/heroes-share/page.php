<?php get_header(); ?>

<div id="main">
	<div id="page">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

				<div class="form-block">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>

			<?php endwhile; ?>
		<?php else: ?>
			<h2>Not Found</h2>
			<p>Sorry, but you are looking for something that isn't here.</p>
		<?php endif; ?>
	</div>
	
	<div id="page-sidebar">
		<?php get_sidebar('1'); ?>
	</div>
</div>

<?php get_footer(); ?>