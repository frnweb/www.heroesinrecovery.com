<?php
// -------------------------------  Taxonomies ---------------------------------------

// Sample Taxonomy
register_taxonomy(  
	'sample_tax',  
	array('post_type'),  
	array(  
	 'hierarchical' => true,  
	 'label' => 'Sample Taxonomy',  
	 'query_var' => true,  
	 'rewrite' => true  
	)  
); 



//'taxonomies' => array('Louisville, CO', 'New York City, NY', 'Washington, DC', 'Berkeley Springs, WV', 'Roanoke, VA', 'Bristol, VA', 'Knoxville, TN', 'Chattanooga, TN', 'Jackson, TN', 'Memphis, TN', 'Nashville, TN'),
 

?>