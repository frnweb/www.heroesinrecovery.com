<?php
// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// Load jQuery
if ( !is_admin() ) {
   wp_deregister_script('jquery');
   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"), false);
   wp_enqueue_script('jquery');
}

// This theme styles the visual editor with editor-style.css to match the theme style.
add_editor_style();

// This theme uses post thumbnails
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
add_image_size( 'single-post-thumbnail', 400, 9999, true );

// This theme uses wp_nav_menu()
add_theme_support( 'nav-menus' );

// Add default posts and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );

//Change Footer Text
function modify_footer_admin () {
  echo 'Created by <a href="http://ranklab.com">Ranklab</a>';
}
add_filter('admin_footer_text', 'modify_footer_admin');

//Disable Autosave
function disableAutoSave(){
    wp_deregister_script('autosave');
}
add_action( 'wp_print_scripts', 'disableAutoSave' );

//Login Logo
function my_custom_login_logo() {
	echo "
	<style>
	body.login #login h1 a {
		background: url('".get_bloginfo('template_url')."/style/images/wp_login_logo.png') no-repeat scroll center top transparent;
		height: px;
		width: 331px;
	}
	</style>
	";
}
add_action('login_head', 'my_custom_login_logo');
	
//Remove Dashboard Widgets
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

//Unregister Widgets
// unregister all default WP Widgets
function unregister_default_wp_widgets() {
	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Tag_Cloud');
}
add_action('widgets_init', 'unregister_default_wp_widgets', 1);
	

//Replace Excerpt Ellipsis

function replace_excerpt($content) {       
	return str_replace('[...]','<div class="more-link"><a href="'. get_permalink() .'">Continue Reading</a></div>', $content );
	}
	add_filter('the_excerpt', 'replace_excerpt');

?>