<div class="aside-col">
	<div class="recent-posts">
		<h3>RECENT POSTS</h3>
		<?php
			$recent_posts = wp_get_recent_posts( array('numberposts' => 4, 'orderby' => 'post_date', 'post_type' => 'post', 'post_status' => 'publish') );
			foreach( $recent_posts as $recent ){
				echo '<div class="post">';
				echo '<p class="title"><a href="' . get_permalink($recent["ID"]) . '" title="Look '.$recent["post_title"].'" >' .   $recent["post_title"].'</a></p>';
			?>
				<p class="meta">

					Posted on <?php echo date('F jS', strtotime($recent["post_date"])) ?></strong> by <strong><?php echo get_userdata($recent["post_author"])->display_name; ?></strong>
				</p>
				</div>
			<?php
			}			
		?>
	</div>
</div>