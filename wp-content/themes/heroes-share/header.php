<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->
<head>
<meta charset="<?php bloginfo('charset'); ?>">	
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<?php if (is_search()) { ?>
<meta name="robots" content="noindex, nofollow" /> 
<?php } ?>
<title><?php wp_title(''); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/_/img/favicon.ico">
<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/_/img/apple-touch-icon.png">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<!-- all our JS is at the bottom of the page, except for Modernizr. -->
<script src="<?php bloginfo('template_directory'); ?>/library/js/modernizr-1.7.min.js"></script>
<?php wp_head(); ?>
<script type="text/javascript" src="/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/jquery.cycle.all.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("a.hp-video").fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600, 
			'speedOut'		:	470, 
			'overlayShow'	:	false
		});
	});
</script>
<?php if ( $_SERVER["REQUEST_URI"] == '/share-it-with-sean/stories/' ): ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$('#slidecontainer').cycle({ 
			fx:     'fade', 
			timeout: 5000
		});
	});
</script>
<? endif; ?>
<script type="text/javascript">                           
	jQuery(document).ready(function() {
		jQuery('a.share-main').hover(
			function() {
				jQuery('.share-drop').show();
			},
			function() {
				if (jQuery('.share-drop').is(':visible')) {
					setTimeout(function() {  
						if (navigator.appName == 'Microsoft Internet Explorer') {
							if (jQuery('.share-drop:hover').length == 0) { jQuery('.share-drop').hide(); }
						}
						else {
							if (!jQuery('.share-drop').is(':hover')) { jQuery('.share-drop').hide(); }
						}
					}, 50);
				}
			}
		);
		
		jQuery('.share-drop').hover(
			function() { },
			function() {
				setTimeout(function() {  
					if (navigator.appName == 'Microsoft Internet Explorer') {
						if (jQuery('a.share-main:hover').length == 0) { jQuery('.share-drop').hide(); }
					}
					else {
						if (!jQuery('a.share-main').is(':hover')) { jQuery('.share-drop').hide(); }
					}
				}, 50);
			}
		);
	});
</script>
</head>
<body <?php body_class(); ?>>
<!--wrapper-->
<div id="wrapper">
	<div class="wrapper-holder">
		<div class="wrapper-frame">
			<div class="w1">
				<div id="header">
					<h1 class="logo"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
					<!--<?php get_search_form(); ?>-->
					<div class="section">
																				
						<ul class="heroes-nav">
						<li><a href="http://www.heroesinrecovery.com/stories/diana/" class="btn-h">H</a></li>
						<li><a href="http://www.heroesinrecovery.com/stories/allen/" class="btn-e">E</a></li>
						<li><a href="http://www.heroesinrecovery.com/stories/margaret/" class="btn-r">R</a></li>
						<li><a href="http://www.heroesinrecovery.com/stories/mike/" class="btn-o">O</a></li>
						<li><a href="http://www.heroesinrecovery.com/stories/tracy/" class="btn-ee">EE</a></li>
						<li><a href="http://www.heroesinrecovery.com/stories/morgan/" class="btn-s">S</a></li>
						</ul>
					
					</div>
					<!-- end section -->
					<div class="panel">
						<ul class="social">
							<li><a href="https://www.facebook.com/HeroesinRecovery" target="_blank" class="btn-facebook" title="Follow FRN on Facebook">facebook</a></li>
							<li><a href="<?php bloginfo('rss2_url'); ?>" class="btn-rss">rss</a></li>
							<li><a href="http://twitter.com/#!/HeroesNRecovery" class="btn-twitter" target="_blank">twitter</a></li>
							<li><a href="http://www.youtube.com/user/foundationsrnetwork" target="_blank" class="btn-youtube" title="Follow FRN on YouTube">youtube</a></li>
						</ul>
						<strong class="txt-recovery">IN RECOVERY tm</strong>
						<!--<a href="#" class="btn-connect">connect</a>-->
						
					</div>
					<ul id="nav" class="">
						<li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page active page_item page-item-5 current_page_item menu-item-34"><a href="http://www.heroesinrecovery.com/">HOME</a></li>
						<li id="menu-item-654" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-654"><a href="http://www.heroesinrecovery.com/about/">ABOUT</a>
						<div class="drop">
						<div class="drop-c">
						<ul>	<li id="menu-item-663" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-663"><a href="http://www.heroesinrecovery.com/logo-inspiration/">LOGO INSPIRATION</a></li>
							<li id="menu-item-806" class="last menu-item menu-item-type-post_type menu-item-object-page menu-item-806"><a href="http://www.heroesinrecovery.com/lead-advocates/">LEAD ADVOCATES</a></li>
						</ul>
						</div>
						</div>
						</li>
						<li id="menu-item-1025" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1025"><a href="http://www.heroesinrecovery.com/stories">INSPIRING STORIES</a></li>
						<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="http://www.heroesinrecovery.com/share/">SHARE STORY</a></li>
						<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="http://www.heroesinrecovery.com/blog/">HEROES BLOG</a></li>
						<li id="menu-item-181" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-181"><a href="/events/">NEWS &#038; EVENTS</a></li>
						<li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><a title="Need to Talk? We’re Here to Listen." href="http://www.heroesinrecovery.com/help/">CONTACT</a></li>
					</ul>						
					<div class="share-it">
						<a class="share-main" href="/share-it-with-sean/"></a>
						<div class="share-drop">
							<ul>
								<li class="share-stories"><a href="/share-it-with-sean/stories/">Sean's Stories</a></li>
								<li class="share-blog"><a href="/share-it-with-sean/blog/">Sean's Blog</a></li>
							</ul>
						</div>
					</div>
				</div><!-- end header -->
		