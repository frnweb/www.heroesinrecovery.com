<?php 
/**
 * Template Name: Contact
 */

get_header(); 
?>
<?php get_template_part('_page-top'); ?>
<div class="page-row row">
	<article class="page-content-wrap col-sm-8 col-sm-offset-2">
		<div class="row page-content-row">
			<div class="col-sm-8 contact-form">
				<?php echo get_field('form_shortcode'); ?>
			</div><!-- /.contact-form -->
			<div class="col-sm-4 contact-info">
				<div class="contact-phone">
					<strong>Phone</strong><br>
					<?php echo do_shortcode('[frn_phone number="'.get_field('phone_number').'"]'); ?>
<!--<a href="tel:<?php //echo get_field('phone_number'); ?>"><?php //echo get_field('phone_number'); ?></a>-->
				</div>
				<div class="contact-address">
					<strong>Address</strong>
					<?php echo get_field('address'); ?>
				</div>
				<div class="contact-map">
					<?php echo get_field('map_embed_code'); ?>
				</div>
			</div><!-- /.contact-info -->
		</div><!-- /.page-content-row -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>