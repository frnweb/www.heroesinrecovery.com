<div class="comments">
	<?php if (post_password_required()) : ?>
	<p>Post is password protected. Enter the password to view any comments.</p>	
</div>	
	<?php return; endif; ?>
<?php if (have_comments()) : ?>
	<h2><?php comments_number(); ?></h2>
	<ul><?php wp_list_comments('type=comment'); ?></ul>
<?php elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
	<p>Comments are closed.</p>
<?php endif; ?>
</div>
<div class="row">
	<div class="col-sm-9 single-comments">
		<?php comment_form(); ?>
	</div>
</div>
