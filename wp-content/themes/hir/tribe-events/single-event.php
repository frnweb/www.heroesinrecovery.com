<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$event_id = get_the_ID();
?>
<div class="page-row row">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="tabbed-content">
			<!--
				// About Us
			-->
			<div id="tab-1" class="tab-wrap" data-tab="about">
				<?php the_content(); ?>
			</div><!-- /#tab-1 -->
		
		</div><!-- /.tabbed-content -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>
