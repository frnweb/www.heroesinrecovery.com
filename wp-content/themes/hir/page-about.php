<?php
/**
 * Template Name: About Us
 */

get_header();
?>
<?php /* get_template_part('_page-top');  */ ?>

<div class="page-row row">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="tabbed-content">
			<!--
				// About Us
			-->
			<div id="tab-1" class="tab-wrap" data-tab="about">
				<?php echo get_field('about_content'); ?>
			</div><!-- /#tab-1 -->
			<!--
				// News
			-->
			<div id="tab-1" class="tab-wrap" data-tab="news">
				<div class="row">
					<h2 class="tab-h2"><?php echo get_field('news_section_title') ? get_field('news_section_title') : 'News'; ?></h2>
                    <?php

                    $args = array(
                        'post_type'              => 'news',
                        'post_status'            => 'publish',
                        'posts_per_page'         => 10,
                        'order'                  => 'DESC',
                        'paged'                  => $paged,
                    );

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) {
                        
                        while ( $query->have_posts() ) {
                            $query->the_post();
                     ?>
					<article id="post-<?php the_ID(); ?>" class="blog-post-row news-post-row row">	
						<div class="blog-post-loop-img col-sm-1">			
							<?php the_post_thumbnail(); ?>
						</div><!-- /.blog-post-loop-img -->									
						<div class="blog-post-loop-content col-sm-11">
							<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							<div class="blog-post-loop-meta">
								<?php the_date(); ?>
							</div><!-- /.blog-post-loop-meta -->
							<?php the_excerpt(); ?>
						</div><!-- /.blog-post-loop-content -->
					</article><!-- /.row -->
                    <?php
                        }
                    } else {
                        echo 'No News Found';
                    }        

                    // Restore original Post Data
                    wp_reset_postdata(); 
                    ?>   
				</div>
			</div><!-- /#tab-1 -->			
			<!--
				// Advocates
			-->
			<div id="tab-2" class="tab-wrap" data-tab="advocates">				
				<div class="row advocates-row">			
					<h2 class="tab-h2" style="color: #d36147;"><?php echo get_field('advocates_section_title') ? get_field('advocates_section_title') : 'Advocates'; ?></h2>
					<div class="advocates col-sm-12">
						<?php
						if( have_rows('advocate_information') ):
							$i = 1;
						    while ( have_rows('advocate_information') ) : the_row();
						?>	
							<div class="advocate-wrap row">					
								<div class="col-sm-3 advocate-img">
									<img src="<?php echo the_sub_field('advocate_image'); ?>" alt="<?php echo the_sub_field('advocate_name'); ?>">
								</div><!-- /.advocate-img -->
								<div class="col-sm-9 advocate-content">
									<h2><?php echo the_sub_field('advocate_name'); ?></h2>
									<?php echo the_sub_field('advocate_bio'); ?>
								</div><!-- /advocate-content -->
							</div><!-- /.advocate-wrap -->
				        <?php
				        	$i++;
						    endwhile;

						else :
							echo 'Nothing Found';
						endif;

						?>		
					</div><!-- /.hide-reveal -->
				</div><!-- /.row -->
			</div><!-- /#tab-2 -->
			<!--
				// FAQs
			-->
			<div id="tab-3" class="tab-wrap" data-tab="faqs">				
				<div class="row">
					<h2 class="tab-h2"><?php echo get_field('faq_section_title') ? get_field('faq_section_title') : 'FAQs'; ?></h2>
					<div class="col-sm-12 expand-btn-wrap">
						<button class="expand-btn">Expand All</button>
					</div><!-- /.expand-btn-wrap -->
				</div><!-- /.row -->
				<div class="row hide-reveal-row">
					<div class="hide-reveal col-sm-12">
						<?php
						if( have_rows('faq') ):
							$i = 1;
						    while ( have_rows('faq') ) : the_row();
						?>	
							<div class="hide-reveal-wrap row">					
								<div class="hide-reveal-question col-sm-9">
									<h4><?php echo the_sub_field('question'); ?></h4>
								</div><!-- /.hide-reveal-question -->
								<div class="hide-reveal-read-more col-sm-3">
									<button class="hide-reveal-read-more-button" data-show="<?php echo $i; ?>"><span><?php echo $moreText; ?></span> <i class="fa fa-angle-down"></i></button>
								</div><!-- /.hide-reveal-read-more -->
								<div class="hide-reveal-answer col-sm-12" data-answer="<?php echo $i; ?>">
									<?php echo the_sub_field('answer'); ?>
								</div><!-- /.hide-reveal-answer -->
							</div><!-- /.hide-reveal-wrap -->
				        <?php
				        	$i++;
						    endwhile;

						else :
							echo 'Nothing Found';
						endif;

						?>		
					</div><!-- /.hide-reveal -->
				</div><!-- /.row -->
			</div><!-- /#tab-3 -->			
		</div><!-- /.tabbed-content -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>
<?php
if( $_GET['tab'] == 'news' )
{
?>
<script>
jQuery(function($) {
	$('a[data-target="news"]').trigger("click");
})
</script>
<?php
}
?>