<!-- pagination -->
<div class="col-sm-12 no-padding pagination">
	<div class="col-sm-6 post-nav-link no-padding">
		<?php echo previous_posts_link( '&laquo; Previous Page' ); ?>
	</div>			
	<div class="col-sm-6 post-nav-link no-padding" style="text-align: right;">
		<?php echo next_posts_link( 'Next Page &raquo;' ); ?>
	</div>		
</div>	
<!-- /pagination -->