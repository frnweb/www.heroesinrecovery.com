<?php
if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) || is_singular('stories') )
{
	$search = 'Search All Stories';
	$hidden = '<input type="hidden" name="post_type" value="stories" />';
}
else
{
	$search = 'Search All Blog Posts';
	$hidden = '';
}
?>
<div class="row search-row">
	<div class="col-sm-10 col-sm-offset-1">
		<div class="row">
			<div class="col-sm-9">
				<?php if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) || is_singular('stories') ): ?>
					<?php
					$current = get_query_var('story_category');

					$terms = get_terms('story_category');

					if( $terms )
					{
						echo '<ul class="category-list">';
						foreach( $terms as $term )
						{
							if($current == $term->slug)
							{
								$classes = ' class="current-cat"';
							}
							else
							{
								$classes = '';
							}
							echo '<li><a href="'.get_bloginfo('url').'/story_category/'.$term->slug.'" title="'.$term->name.'"'.$classes.'>'.$term->name.'</a></li>';
						}
						echo '</ul>';
					}
					?>
				<?php elseif( is_singular('news') ): ?>
				<div class="news-pagination">
					<a href="<?php echo get_bloginfo('url'); ?>/about">About</a> > <a href="<?php echo get_bloginfo('url'); ?>/about/#news">News</a>
				</div><!-- /news-pagination -->
				<?php elseif( is_single( get_the_id() ) ): ?>
				<div class="news-pagination">
					<a href="<?php echo get_bloginfo('url'); ?>/blog">Blog</a> > <?php the_title(); ?>
				</div>
				<?php endif; ?>
			</div><!-- /.col-sm-6 -->
			<div class="col-sm-3">
				
				<div class="search-form-wrap">
					<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">						
						<input class="search-input" type="search" name="s" placeholder="<?php echo $search; ?>">
						<?php echo $hidden; ?>
						<button class="search-submit" type="submit" role="button">
							<span class="sr-only">Search</span><i class="fa fa-search"></i>
						</button>
						<?php if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) || is_singular('stories') ): ?>
						<div class="story-additional">
							<div class="faux-checkbox"><input type="checkbox" name="a" value="true"></div> Author<br>
							<div class="faux-checkbox"><input type="checkbox" name="k"></div> Keyword<br>
							<div class="faux-checkbox"><input type="checkbox" name="t"></div> Title<br>
							<a href="#" class="search-close">Close</a>
						</div><!-- /.story-additional -->				
						<?php endif; ?>					
					</form>					
				</div><!-- /.search-form-wrap -->				
			</div><!-- /.col-sm-6 -->
		</div><!-- /.row -->
	</div>
</div><!-- /.search-row -->
