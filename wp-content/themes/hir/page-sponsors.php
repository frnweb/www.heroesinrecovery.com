<?php
/**
 * Template Name: Sponsors
 */

get_header();
?>
<?php get_template_part('_page-top'); ?>

<div class="orange-heroes-row row">
	<div class="col-sm-12 col-md-5" id="blue-heroes">
		<img src="<?php echo get_field('sponsor_image'); ?>" alt="Featured Partner Logo">
	</div>				
	<div class="col-sm-12 col-md-7" id="blue-heroes">
		<h4><?php echo get_field('sponsor_title'); ?></h4>
		<?php echo get_field('sponsor_copy'); ?>
		<p style="padding: 15px 15px 0 15px"><a href="<?php echo get_field('sponsor_link'); ?>" target="_blank" class="hir-btn hir-btn-blue2 slim"><?php echo get_field('sponsor_button_text'); ?></a></p>
	</div><!-- /#submit-content -->	
</div><!-- / 2nd row -->

<div class="page-row row partners-row">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="row page-content-row" align="center">
			<?php the_content(); ?>

				<?php if(get_field('all_sponsor_logos')): ?>

						<?php while(has_sub_field('all_sponsor_logos')): ?>
							<div class="partners_logo">
							<div class="size_protector">
							<a href="<?php the_sub_field('sponsor_link'); ?>">
							<img class="author_image" src="<?php the_sub_field('sponsors_logos'); ?>">
							</a>
							</div>
							</div>

						<?php endwhile; ?>

					<?php endif; ?>

		</div><!-- /.page-content-row -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>