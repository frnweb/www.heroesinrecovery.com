<?php
//Secondary nav

if(is_page("About")) :
?>
<div class="tab-nav-row row">
	<nav class="tab-nav col-sm-10 col-sm-offset-1">
		<ul>
			<li><a href="#about" title="About" class="tab-link active" data-target="about">About</a></li>
			<li><a href="#news" title="News" class="tab-link" data-target="news">News</a></li>
			<!-- <li><a href="#advocates" title="Advocates" class="tab-link" data-target="advocates">Meet Our Team</a></li> -->
			<li><a href="#faq" title="FAQ" class="tab-link" data-target="faqs">FAQ</a></li>
		</ul>
	</nav><!-- /.tab-nav -->
</div><!-- /.row -->

<?php elseif(is_page_template('page-event.php')) : ?>

<div class="tab-nav-row row">
	<nav class="tab-nav col-sm-10 col-sm-offset-1">
		<ul>
			<li><a href="#about" title="About" class="tab-link active" data-target="about">About</a></li>
			<li><a href="#charity" title="Charity" class="tab-link" data-target="charity">Charity</a></li>
			<li><a href="#volunteer" title="Volunteer" class="tab-link" data-target="volunteer">Volunteer</a></li>
			<li><a href="#course" title="Course" class="tab-link" data-target="course">Course</a></li>
			<li><a href="#schedule" title="Schedule" class="tab-link" data-target="schedule">Schedule</a></li>
			<li><a href="#awards" title="Awards &amp; Results" class="tab-link" data-target="awards">Awards &amp; Results</a></li>
			<li><a href="#photos" title="Photos" class="tab-link" data-target="photos">Photos</a></li>
			<li><a href="#faq" title="FAQ" class="tab-link" data-target="faq">FAQ</a></li>
		</ul>
	</nav><!-- /.tab-nav -->
</div><!-- /.row -->
<?php

endif;

?>