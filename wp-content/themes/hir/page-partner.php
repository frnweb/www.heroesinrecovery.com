<?php
/**
 * Template Name: Partner
 */

get_header();
?>
<?php get_template_part('_page-top'); ?>

<div class="orange-heroes-row row">
	<div class="col-sm-12 col-md-5" id="blue-heroes">
		<img src="<?php echo get_field('partner_logo'); ?>" alt="Featured Partner Logo">
	</div>				
	<div class="col-sm-12 col-md-7" id="blue-heroes">
		<h4><?php echo get_field('partner_title'); ?></h4>
		<?php echo get_field('partner_copy'); ?>
		<?php $partner_link = get_field('partner_link'); ?>
		<p style="padding: 15px 15px 0 15px"><a href="<?=get_author_posts_url($partner_link['ID']); ?>" class="hir-btn hir-btn-blue2 slim"><?php echo get_field('partner_button_text'); ?></a></p>
	</div><!-- /#submit-content -->	
</div><!-- / 2nd row -->

<div class="page-row row partners-row">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="row page-content-row" align="center">
			<?php the_content(); ?>

			<?php
				//get list of authors
				//loop through
				//pull gravatar mages for each author
				/*
				$args = array(
				'blog_id'      => $GLOBALS['blog_id'],
				'role'         => '',
				'role__in'     => array(),
				'role__not_in' => array(),
				'meta_key'     => 'author_type',
				'meta_value'   => '1',
				'meta_compare' => '',
				'meta_query'   => array(),
				'date_query'   => array(),        
				'include'      => array(),
				'exclude'      => array(),
				'orderby'      => 'login',
				'order'        => 'ASC',
				'offset'       => '',
				'search'       => '',
				'number'       => '',
				'count_total'  => false,
				'fields'       => 'all',
				'who'          => ''
				 ); 
				$partners_array=get_users( $args );
				
				echo "<h1>".count($authors)."</h1>";
				print_r($partners_array);

				*/


				// WP_User_Query arguments
				$args = array (
				    'meta_query' => array(
				        'relation' => 'OR',
				        array(
				            'key'     => 'author_type',
				            'value'   => '1',
				            'compare' => 'LIKE'
				        )
				    )
				);

				// Create the WP_User_Query object
				$wp_user_query = new WP_User_Query($args);
				$authors = $wp_user_query->get_results();
				//echo "<h1>".count($authors)."</h1>";
				//print_r($authors);

				foreach($authors as $author) { 

					//as of 10/27/16, although WP documentation for get_avatar says that we should be able to change alt tag and class (among other things), me and dozens of others online cannot use their info and change the class or alt tags. I didn't like other's options using regex, so I moved to a DOM approach to replace attribute values.
					$author_avatar_html = get_avatar( $author->user_email, 512);

					try {
						//////////
						// Initiate DOM system
						//"TRY" basically allows us to move things forward as normal if something errors out in our process (i.e. users won't know the difference)
						//To add blocks around blocks of content, we need to initial direct object modeling
						//requires PHP 5 or newer
						
						//Prep the code for DOM parsing//////
						libxml_use_internal_errors(true); //indicates that you're going to handle the errors and warnings yourself and you don't want them to mess up the output of your script
						$author_avatar_dom = new DOMDocument(null, 'UTF-8');  //makes sure that the character code matches WordPress's PHP character code since things aren't always stored the same in the DB
						@$author_avatar_dom->loadHTML($author_avatar_html);  //the @ indicates we don't want it to throw errors and instead just store them and keep processing
						libxml_clear_errors(); //Keeps the loadHTML part from throwing unnecessary formatting errors  //good post on this: http://stackoverflow.com/questions/1148928/disable-warnings-when-loading-non-well-formed-html-by-domdocument-php
						$author_avatar = $author_avatar_dom->getElementsByTagName('img')->item(0); //this might be unnecessary, but didn't want to test. It's not a heavy process since the code is so small
						//End DOM Prep//////

						$author_avatar->setAttribute('class',"author_image");
						$author_avatar->setAttribute('alt',$author->display_name);

						//Save changes and reset image html
						$author_avatar_html=$author_avatar_dom->saveHTML();
				
					} catch (Exception $e) {
						//if there is an error message stored by PHP, then just cancel it all and return the normal avatar code to avoid any obvious error to user
						//$author_image = $author_avatar_html;
					}

					?>

					<div class="partners_logo"><a href="/blog/author/<?=$author->user_login;?>/" >
						<div class="size_protector"><?=$author_avatar_html;?></div>
					</a></div>

				<?php }


			?>

		</div><!-- /.page-content-row -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>