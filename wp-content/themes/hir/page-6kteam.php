<?php
/**
 * Template Name: 6K Team
 */

get_header();
?>

<!--<div class="tab-nav-row row">
	<nav class="tab-nav col-sm-10 col-sm-offset-1">
		<ul>
			<li><a href="#about" title="About" class="tab-link active" data-target="about">About</a></li>
			<li><a href="#news" title="News" class="tab-link" data-target="news">News</a></li>
			<li><a href="#advocates" title="Advocates" class="tab-link" data-target="advocates">Meet Our Team</a></li>
			<li><a href="#faq" title="FAQ" class="tab-link" data-target="faqs">FAQ</a></li>
		</ul>
	</nav> /.tab-nav
</div> /.row -->
<div class="page-row row">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="tabbed-content">
			<!--
				// About Us
			-->			
			<!--
				// Advocates
			-->
			<div id="tab-2" class="tab-wrap" data-tab="advocates">				
				<div class="row advocates-row">
					<h2 class="tab-h2" style="color: #d36147;"><?php echo get_field('advocates_section_title') ? get_field('advocates_section_title') : 'Advocates'; ?></h2>
					<div class="advocates col-sm-12">
						<?php
						if( have_rows('advocate_information') ):
							$i = 1;
						    while ( have_rows('advocate_information') ) : the_row();
						?>	
							<div class="advocate-wrap row">					
								<div class="col-sm-3 advocate-img">
									<img src="<?php echo the_sub_field('advocate_image'); ?>" alt="<?php echo the_sub_field('advocate_name'); ?>">
								</div><!-- /.advocate-img -->
								<div class="col-sm-9 advocate-content">
									<h2><?php echo the_sub_field('advocate_name'); ?></h2>
									<?php echo the_sub_field('advocate_bio'); ?>
								</div><!-- /advocate-content -->
							</div><!-- /.advocate-wrap -->
				        <?php
				        	$i++;
						    endwhile;

						else :
							echo 'Nothing Found';
						endif;

						?>		
					</div><!-- /.hide-reveal -->
				</div><!-- /.row -->
			</div><!-- /#tab-2 -->
			<!--
				// FAQs
			-->
			
		</div><!-- /.tabbed-content -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>