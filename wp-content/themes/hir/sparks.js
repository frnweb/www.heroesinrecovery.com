jQuery(function($) {

	// Screen Width
	var screenWidth = $( window ).width();

	/**
	 * Functions to call when window width changes
	 */
	$(window).on("resize", function() {
		// Update width
		oldWidth = screenWidth;
		screenWidth = $( window ).width();
		
		if( screenWidth > 1200 )
		{
			setTimeout(function() {
				$("#header-row").sticky({
					responsiveWidth: true,
					getWidthFrom: 'body',
					className: 'stuck',
					topSpacing: 0
				});
			}, 600);
		}
		else
		{
			setBodyPadding();
		}


		$("#header-row-sticky-wrapper").attr('style', '');

		gridHeight();

		if( screenWidth < 1200 )
		{
			moveHeroImg();
		}
		else
		{
			removeHeroImg();
		}

		if( screenWidth > oldWidth ) {

		}

		$('.story-item-link img').height($('.story-item-link img').width());

		if( screenWidth < 768 ) {
			mobileFunctions();
		} else {
			revertMobile();
		}

		video169();

		// Fix fonts
		// causeRepaintsOn = $("h1, h2, h3, p, div, input");
		// causeRepaintsOn.css("z-index", 1);
	});

	$(window).on('scroll', function() {
		var $header = $('#header'),
			top     = $header.offset().top;

		clearTimeout($.data(this, 'scrollTimer'));
		$.data(this, 'scrollTimer', setTimeout(function() {
			if( screenWidth > 1200 )
			{
				if( top > 0 && top < 50 )
				{
					$('*').removeClass('stuck');
				}
			}
			else
			{
			
			}
		}, 100));

		if($('.floating-stories').length > 0)
		{
			var $float = $('.floating-stories'),
				ftop   = $float.offset().top,
				fh     = $float.height(),
				ht     = $('#header').offset().top,
				ft     = $('#footer').offset().top;

			if( screenWidth >= 768 )
			{
				if( ht > 200 )
				{
					$float.fadeOut();
				}
				else
				{
					$float.fadeIn();
				}
			}
			else
			{
				if( ht > 1000 )
				{
					$float.fadeOut();
				}
				else
				{
					$float.fadeIn();
				}
			}
		}

	});

	/**
	 * Mobile Functions
	 */
	function mobileFunctions() {
		makeHamburger();
		makeBgImg();
	}

	/**
	 * Functions to call to revert mobile stuff
	 */
	function revertMobile() {
		eatHamburger();
	}

	function goTo( el ) {

		$( 'html, body' ).animate( {
			scrollTop : ( ( $( el ).offset().top - 100 ) )
		}, 500);

	}

	function makeBgImg() {
		var $img = $('.has-img .page-top-events img').last(),
			src = $img.attr('src');

		// Insert bg
		$('.has-img .page-top-events').css('backgroundImage', 'url('+src+')');

		$img.css('opacity', 0);
	}

	function removeBgImg() {
		$('.has-img .page-top-events').css('backgroundImage', '');
		$img.css('opacity', 0);
	}

	/**
	 * Makin' Hamburgers
	 */
	function makeHamburger() {
		$( '#header-nav' ).hide();

		if( $( '#hamburger-nav' ).length === 0 ) {
			var $container = $( '#header-nav ul' ),
				$links = $( '#header-nav ul li a' ),
				html;

			html  = '<div id="hamburger-nav"><div id="hamburger-nav-inner">';
			var id;
			
			$links.each( function() {
				var $this = $( this ),
					sub = $(this).parents('ul').hasClass('sub-menu'),
					classes = $this.parent('li').attr('class');
					
					if(sub)
					{
						classes += ' child-page';
					}

				html += '<a href="'+ $this.attr( 'href' ) +'" class="nav-button nav-button-hamburger '+classes+'">';
				html += $this.text();
				html += '</a>';
			} );

			html += '</div></div>';
			html += '<button id="menu-button"><i class="fa fa-bars menu-icon"></i></button>';

			$( '#header' ).prepend( html );
		}
	}

	function eatHamburger() {
		$( '#menu-button' ).hide();
		$( '#hamburger-nav' ).remove();
		$( '#header-nav' ).show();
	}



	function video169() {
		//This function changes height for all videos on a page -- it automatically loops for all cases
		var $video = $('#who-we-are iframe, #green-heroes-video iframe, .page-content-wrap iframe');
		$video.height( $video.width() * 9/16 );
		
		$video.each(function(index){
			if($( this ).attr('src').indexOf("wufoo")>-1) alert("wufoo found");
			//else $video.height( $video.width() * 9/16 );
		});

		
	}

	function videoList169() {
		var $video = $('.video-wrap iframe');
		$video.height( $video.width() * 9/16 );
	}

	function forceHeroImageSize() {
		console.log('Resizing Images...');
		$('.story-item-link img').each(function() {
			$this = $(this);
			$this.height($this.width());
		});
	}

	function moveHeroImg() {
		if($('.mobile-featured-hero').length < 1)
		{
			var $clone = $('#featured-hero-img img').clone();

			$clone.addClass('mobile-featured-hero');

			$('#featured-hero-copy h2').after($clone);
		}
	}

	function removeHeroImg() {
		$('.mobile-featured-hero').remove();
	}

	function gridHeight() {

		if( screenWidth > 1200 )
		{
			var h  = parseInt($('.ri-grid ul li').height()),
				pt = parseInt($('#image-grid-row').css('paddingTop')),
				pb = parseInt($('#image-grid-row').css('paddingBottom')),
				n = (h*3)-pt-pb;

			$('#image-grid-row').height(n);
		}

	}

	function setBodyPadding() {
		$('#main-content').css('paddingTop', $('#header-row').height());
	}



	/**
	 * Call on Load
	 */
	video169();
	
	if( screenWidth > 1200 ) {
		$("#header-row").sticky({
			responsiveWidth: true,
			getWidthFrom: '#header-row',
			className: 'stuck',
			topSpacing: 0
		});
	}
	else
	{
		setBodyPadding();
	}

	if( $( '#ri-grid' ).length > 0 && screenWidth > 1024 )
	{
		$( '#ri-grid' ).gridrotator( {
			rows : 3,
			columns : 8,
			animType: 'fadeInOut',
			maxStep : 5,
			interval : 1500,
			w1200 : {
				rows : 3,
				columns : 4
			},
			w1024 : {
				rows : 3,
				columns : 5
			},
			w768 : {
				rows : 3,
				columns : 5
			},
			w480 : {
				rows : 3,
				columns : 2
			},
			w320 : {
				rows : 3,
				columns : 2
			},
			w240 : {
				rows : 3,
				columns : 2
			},
		} );

		setTimeout(function() {
			gridHeight();
		}, 2000);

	}

	if( screenWidth < 768 ) {
		mobileFunctions();
	} else {
		revertMobile();
	}
	 
	$( 'body' ).on('click', '#menu-button', function(e) {
		e.preventDefault();

		$('#hamburger-nav').slideToggle();

		if($('.menu-icon').hasClass('fa-bars'))
		{
			$('.menu-icon').removeClass('fa-bars').addClass('fa-times');
				if( screenWidth < 769 ) {
					$('body,html').css({
						'position' : 'fixed',
						'width'    : '100%'
					});
				}
		}
		else
		{
			$('.menu-icon').removeClass('fa-times').addClass('fa-bars');
				if( screenWidth < 769 ) {
					$('body,html').css('position', 'relative');
				}
		}
	});

	$('#header-nav li').hover(function() {
		var $dropdown = $(this).children('.sub-menu');

		if($dropdown.hasClass('is-open')) {
			$dropdown.removeClass('is-open');
		} else {
			$dropdown.addClass('is-open');
		}
	});

	/**
	 * Tabs
	 */
	function goToTab() {
		var href = window.location.href,
			regex = /(#[a-z\d][\w-]*)/ig,
			match = href.match(regex);

		if( match[0] )
		{
			$('a[href="'+match[0]+'"]').click();
		}
	}

	if( $('.tab-wrap').length > 0 )
	{
		// Hide all tabs
		$('.tab-wrap').hide();

		// Then display the first one
		$('.tab-wrap').first().show();

		$('.tab-link').click(function(e) {

			var $link = $(this),
				target = $link.attr('data-target'),
				$requested = $('.tab-wrap[data-tab="'+target+'"]');


			// Remove "active" class from all links
			$('.tab-link').removeClass('active');

			// Add it to the link just clicked
			$link.addClass('active');

			// If the reuqested tab is currently visible, do nothing.
			if( !$requested.is(':visible') )
			{
				// Fade out visible tab
				// Fade in requested tab
				$('.tab-wrap:visible').fadeOut(function() {
					$requested.fadeIn(function() {
						if(screenWidth < 768)
						{
							goTo('.page-row');
						}
					});
				});
			}

			if(screenWidth < 768)
			{
				goTo('.page-row');
			}

		});

		// See if we have an anchor in the url
		// if so, go to that tab
		goToTab();

		window.onhashchange = function() {
			goToTab();
		};
	}

	/**
	 * Hide/Reveal
	 */
	if( $('.hide-reveal-wrap').length > 0 )
	{
		var sliding = false;

		$('.hide-reveal-wrap').click(function() {
			if(!sliding)
			{
				$(this).find('.hide-reveal-read-more-button').trigger('click');
			}
		});

		$('.hide-reveal-read-more-button').click(function() {
			var id = $(this).attr('data-show'),
				$icon = $(this).children('i');

			sliding = true;

			if($icon.hasClass('fa-angle-down')) {
				$icon.removeClass('fa-angle-down')
					.addClass('fa-angle-up');

				$(this).parents('.hide-reveal-wrap').addClass('faq-active');
			} else {
				$icon.removeClass('fa-angle-up')
					.addClass('fa-angle-down');
				$(this).parents('.hide-reveal-wrap').removeClass('faq-active');
			}

			$('.hide-reveal-answer[data-answer="'+id+'"]').slideToggle(function() {
				sliding = false;
			});
		});

		$('.expand-btn').click(function() {
			
			if($(this).text() == 'Expand All') {
				$(this).text('Collapse All');
				$('.hide-reveal-answer').slideDown(function() {
					$(this).parent('.hide-reveal-wrap').addClass('faq-active');
				});				
			} else {
				$(this).text('Expand All');
				$('.hide-reveal-answer').slideUp(function() {
					$(this).parent('.hide-reveal-wrap').removeClass('faq-active');
				});
			}

		});
	}

	/**
	 * Faux Checkboxes
	 */
	if( $('.faux-checkbox').length > 0 )
	{
		
		// Make sure everything is synched
		$('.faux-checkbox').each(function() {
			var $faux = $(this),
				$real = $(this).children('input[type="checkbox"]');

			if( $real.prop('checked') === true )
			{
				$faux.addClass('checked');
			}
		});

		$('.faux-checkbox').click(function() {
			var $faux = $(this),
				$real = $(this).children('input[type="checkbox"]');

			if( $faux.hasClass('checked') )
			{
				$faux.removeClass('checked');
				$real.prop('checked', false);
			}
			else
			{
				$faux.addClass('checked');
				$real.prop('checked', true);
			}
		});
	}

	/**
	 * Sharing Functionality
	 */
	if( $('.share-row').length > 0 )
	{

		// Scroll to errors
		if( $('.errors-row').length > 0 )
		{
			goTo('.errors-row');
		}

		// Show section
		$('.select-step-link').click(function(e) {
			e.preventDefault();

			var $link  = $(this),
				target = $link.attr('data-show'),
				$show  = $('[data-step="'+target+'"]');

			// Populate hidden form field
			$('#share-story-type').val(target);

			// If the other set of instructions are visible, hide'em
			$('[data-step]').not('[data-step="'+target+'"]').fadeOut();

			// Fade out instructions
			$('.step-4-select').fadeOut(function() {
				$('#share-form-wrap').slideDown();
				$show.fadeIn(function() {
					// If this is a button and not a text link add an active class
					$('[data-show]').removeClass('step-active');
					$('.faux-btn').removeClass('step-after-active');

					if($show.is(':visible') && ( $link.hasClass('hir-btn') || $link.hasClass('hir-btn-button')  ) )
					{
						$link.addClass('step-active');

						// Faux Btn
						if($link.parents('.faux-btn').length > 0)
						{
							$link.parents('.faux-btn').addClass('step-after-active');
						}
					}
					// If text links are clicked, add active classes to buttons
					else if($show.is(':visible') && (!$link.hasClass('hir-btn') || !$link.hasClass('hir-btn-button')))
					{
						$('.hir-btn-button[data-show="'+target+'"]').addClass('step-active');
						$('.hir-btn-button[data-show="'+target+'"]').parents('.faux-btn').addClass('step-after-active');
					}

					goTo('#step-1');
				});
			});

						// Fade out instructions
						$('.step-5-select').fadeOut(function() {
							$('#share-form-wrap').slideDown();
							$show.fadeIn(function() {
								// If this is a button and not a text link add an active class
								$('[data-show]').removeClass('step-active');
								$('.faux-btn').removeClass('step-after-active');
			
								if($show.is(':visible') && ( $link.hasClass('hir-btn') || $link.hasClass('hir-btn-button')  ) )
								{
									$link.addClass('step-active');
			
									// Faux Btn
									if($link.parents('.faux-btn').length > 0)
									{
										$link.parents('.faux-btn').addClass('step-after-active');
									}
								}
								// If text links are clicked, add active classes to buttons
								else if($show.is(':visible') && (!$link.hasClass('hir-btn') || !$link.hasClass('hir-btn-button')))
								{
									$('.hir-btn-button[data-show="'+target+'"]').addClass('step-active');
									$('.hir-btn-button[data-show="'+target+'"]').parents('.faux-btn').addClass('step-after-active');
								}
			
								goTo('#step-1');
							});
						});

		});

		$('.pop-up').fancybox({
			autoWidth : true,
			autoHeight: true
		});

		$('.select-upload').click(function() {
			var btn = $(this).attr('data-button');

			if(btn == 'avatar-btn')
			{
				$('input[name="hero_image"]').val("");
			}
			else
			{
				$('input[name="share-avatar"]').val("");
			}

			$('.hir-btn-button').removeClass('selected-option');
			$('.faux-btn').removeClass('green');

			$('#'+btn).addClass('selected-option');

			$('#'+btn).parent('div').addClass('green');

			$.fancybox.close();
		});

		$('#avatar-list li').click(function(e) {
			e.preventDefault();

			$(this).parent('ul').siblings('.select-upload').click();

			var $selected = $(this),
				val = $selected.find('img').attr('src');

			$('#avatar-list li').removeClass('selected');

			$selected.addClass('selected');

			$('#share-avatar').val(val);

			if( screenWidth < 768 )
			{
				$.fancybox.close();
			}

		});

		if( $('#share-avatar').val() !== '' )
		{
			var url = $('#share-avatar').val();

			$('#avatar-list li a img[src="'+url+'"]').parents('li').addClass('selected');
		}

		if( screenWidth < 768 )
		{
			$('#share-form input[type="text"], #share-form input[type="email"], #share-form textarea').on('focus', function() {
				$('#header-row').fadeOut();
			});

			$('#share-form input[type="text"], #share-form input[type="email"], #share-form textarea').on('blur', function() {
				$('#header-row').fadeIn();
			});
		}

		// Form Validation
		$("#share-form").validate({
			rules: {
				person_name: "required",
				email: "required"
			},
			invalidHandler: function() {
				$('#header-row').unstick();
			}
		});

	}

	if($('.story-additional').length > 0)
	{
		$('.search-input').focus(function() {
			$('.story-additional').slideDown();
		});

		$('.search-close').click(function(e) {
			e.preventDefault();
			$('.story-additional').slideUp();
		});
	}

	if($('.ngg-gallery-thumbnail a').length > 0)
	{
		$('.ngg-gallery-thumbnail a').click(function() {
			var html = '<button class="gallery-close">X</button>';
			$('body').append(html);
		});

		$('body').on("click", '.gallery-close', function() {
			$('#shShutter, #shDisplay').remove();
			$(this).remove();
		});
	}

	if( screenWidth < 1200 )
	{
		moveHeroImg();
	}
	else
	{
		removeHeroImg();
	}

	$.fn.almComplete = function(alm) {
		
		var width = 210;

		$('.story-item-link img').on('load', function() {
			var $this = $(this);

			if( $this.width() < 1 )
			{
				// Min height for images that are slow to load.
				$this.attr('height', width);
			}
			else
			{
				width  = $(this).width();
				$this.height(width);
			}

			gridHeight();
			
		});
	};
 
	$(window).on('load', function() {
		gridHeight();
	});

});
