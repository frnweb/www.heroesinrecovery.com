<?php get_header(); ?>
<?php get_template_part('searchform'); 

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

?>
<div class="page-row row">
	<div class="col-sm-8 col-sm-offset-2">
		<?php

		//// Set Trigger Varibles
		$author_name = get_the_author();
		if($author_name=="") $author_name=$curauth->display_name;
		if(!isset($curauth->user_description)) $bio=""; else $bio = $curauth->user_description;
		if(!isset($curauth->user_url)) {$website=""; $author_type=""; }
		else {
			$website=$curauth->user_url;
			$author_type=$curauth->author_type;
			if($author_type=="0") $author_type="Advocate";
			elseif($author_type=="1") $author_type="Partner";
			elseif($author_type=="2") $author_type="Blogger";
		}



		//Author photo
		if($bio!=="") {
		?>
		<div class="author_meta">
		<div class="author_image_wrap">	
			<?php 
			
			//as of 10/27/16, although WP documentation for get_avatar says that we should be able to change alt tag and class (among other things), me and dozens of others online cannot use their info and change the class or alt tags. I didn't like other's options using regex, so I moved to a DOM approach to replace attribute values.
			$author_avatar_html = get_avatar( $curauth->user_email, 512);

			try {
				//////////
				// Initiate DOM system
				//"TRY" basically allows us to move things forward as normal if something errors out in our process (i.e. users won't know the difference)
				//To add blocks around blocks of content, we need to initial direct object modeling
				//requires PHP 5 or newer
				
				//Prep the code for DOM parsing//////
				libxml_use_internal_errors(true); //indicates that you're going to handle the errors and warnings yourself and you don't want them to mess up the output of your script
				$author_avatar_dom = new DOMDocument(null, 'UTF-8');  //makes sure that the character code matches WordPress's PHP character code since things aren't always stored the same in the DB
				@$author_avatar_dom->loadHTML($author_avatar_html);  //the @ indicates we don't want it to throw errors and instead just store them and keep processing
				libxml_clear_errors(); //Keeps the loadHTML part from throwing unnecessary formatting errors  //good post on this: http://stackoverflow.com/questions/1148928/disable-warnings-when-loading-non-well-formed-html-by-domdocument-php
				$author_avatar = $author_avatar_dom->getElementsByTagName('img')->item(0); //this might be unnecessary, but didn't want to test. It's not a heavy process since the code is so small
				//End DOM Prep//////

				$author_avatar->setAttribute('class',"author_image");
				$author_avatar->setAttribute('alt',$author_name." Profile Photo");

				//Save changes and reset image html
				$author_avatar_html=$author_avatar_dom->saveHTML();
				echo $author_avatar_html;
		
			} catch (Exception $e) {
				//if there is an error message stored by PHP, then just cancel it all and return the normal avatar code to avoid any obvious error to user
				echo $author_avatar_html;
			}
		    
			?>
		</div>
		<?php } 

		//Author name ?>
		<h1 <?=($author_type=="") ? 'class="author_title page-title"' : 'class="author_title" style="margin:0;" ' ; ?>><?=$author_name; ?></h1>
		<?php if($author_type!=="") { ?>
			<div class="author_type"><?=$author_type; ?></div>
		<?php } ?>

		</div>

		<?php

		//Bio
		if($bio!=="") { ?>
		<div class="author_bio">
			<?=$bio; ?>
		</div>
		<?php } 

		if(!isset($curauth->phone)) $phone=""; else $phone=$curauth->phone;
			if($phone!=="") { ?>
		<div class="author_phone"><?=do_shortcode('[frn_phone number="'.$phone.'" class="fa fa-phone" title="Call '.$author_name.'" ]') ;?></div>
				<?php }

		//Website URL
		if($website!=="") { ?>
		<div class="author_url"><a class="fa fa-user" href="<?=$website; ?>"> Visit <?=($author_type!=="") ? "the ".$author_type."'s" : "their " ;?> website</a></div>
		<?php } ?>

		<div class="author_social">
		<?php 

		//Social Links
			//Facebook
				if(!isset($curauth->facebook)) $facebook=""; else $facebook=$curauth->facebook;
				if($facebook!=="") { ?>
			<a class="fa fa-facebook" href="<?=$facebook; ?>" title="<?=$author_name;?>'s Facebook Page"></a>
				<?php }
			//LinkedIn
				if(!isset($curauth->linkedin)) $linkedin=""; else $linkedin=$curauth->linkedin;
				if($linkedin!=="") { ?>
			<a class="fa fa-linkedin" href="<?=$linkedin; ?>" title="<?=$author_name;?>'s Linkedin Page"></a>
				<?php }
			//Google+
				if(!isset($curauth->googleplus)) $googleplus=""; else $googleplus=$curauth->googleplus;
				if($googleplus!=="") { ?>
			<a class="fa fa-google-plus" href="<?=$googleplus; ?>" title="<?=$author_name;?>'s Googleplus Page"></a>
				<?php }
			//Twitter
				if(!isset($curauth->twitter)) $twitter=""; else $twitter=$curauth->twitter;
				if($twitter!=="") { ?>
			<a class="fa fa-twitter" href="<?=$twitter; ?>" title="<?=$author_name;?>'s Twitter Page"></a>
				<?php }
			//Slideshare
				if(!isset($curauth->slideshare)) $slideshare=""; else $slideshare=$curauth->slideshare;
				if($slideshare!=="") { ?>
			<a class="fa fa-slideshare" href="<?=$slideshare; ?>" title="<?=$author_name;?>'s Slideshare Page"></a>
		<?php } ?>
		</div>

		<?php if ( have_posts() ) { ?>
	
		<?php get_template_part('loop'); ?>
		<?php get_template_part('pagination'); ?>
		<?php } ?>
	</div>
</div>
<?php get_footer(); ?>