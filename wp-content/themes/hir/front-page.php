<?php get_header(); ?>
<!-- 
	// Image Grid
-->
<div id="image-grid-row" class="row">
	<section id="image-grid" class="col-sm-8 col-sm-offset-2">
		<div id="image-grid-copy">
			<div id="image-grid-copy-line-1">
				<?php echo get_field('image_grid_copy_line_1'); ?>
			</div><!-- /#image-grid-copy-line-1 -->
			<div id="image-grid-copy-line-2">
				<?php echo get_field('image_grid_copy_line_2'); ?>
			</div><!-- /#image-grid-copy-line-2 -->
			<div id="image-grid-buttons">
				<a href="<?php echo get_field('button_1_link'); ?>" class="hir-btn" title="<?php echo get_field('button_1_text'); ?>"><?php echo get_field('button_1_text'); ?></a>
				<a href="<?php echo get_field('button_2_link'); ?>" class="hir-btn" title="<?php echo get_field('button_2_text'); ?>"><?php echo get_field('button_2_text'); ?></a>
			</div><!-- /#image-grid-buttons -->
		</div><!-- /#image-grid-copy -->
	</section>
	<div id="ri-grid" class="ri-grid ri-grid-size-3">
		<ul>
            <?php

            $args = array(
                'post_type'              => 'stories',
                'post_status'            => 'publish',
                'posts_per_page'         => 45,
				//'orderby' 		 => 'date',
		  		 'order'                  => 'DSC',
                'paged'                  => $paged,
            );

            // The Query
            $query = new WP_Query( $args );

            // The Loop
            if ( $query->have_posts() ) {
                
                while ( $query->have_posts() ) {
                    $query->the_post();

                    if(has_post_thumbnail()) {
             ?>
             <li><a href="#"><?php the_post_thumbnail(); ?></a></li>
            <?php
            		}
                }
            } else {
                echo 'No News Found';
            }        

            // Restore original Post Data
            wp_reset_postdata(); 
            ?> 
				
					
		</ul>
	</div><!-- /#ri-grid -->
</div><!-- /.row -->
<!--
	// Featured Events
-->
<!--
<div id="home-events-row" class="row">
	<section id="home-events" class="col-sm-10 col-sm-offset-1">
		<h2 class="section-h2">Featured Events</h2>
		<table id="home-events-table" itemscope itemtype="http://schema.org/Event">
			<?php
			global $post;
			
			$get_posts = tribe_get_events(array('start_date' => date( 'Y-m-d H:i:s' ),'posts_per_page'=>3,'order'=>'asc', 'orderby'=> 'start_date','tax_query'=> array(array('taxonomy' => 'tribe_events_cat','field' => 'slug','terms' => 'featured'))) );  //removed 'eventDisplay'=>'upcoming' on 6/14/17 since there's a better approach

			foreach($get_posts as $post) { setup_postdata($post); ?> 
				<tr>
					<td class="home-events-date" itemprop="startDate" content="2015-08-01">
						<span class="home-events-date-month"><?php echo tribe_get_start_date($post->ID, true, 'M'); ?></span>
						<span class="home-events-date-day"><?php echo tribe_get_start_date($post->ID, true, 'd'); ?></span>
					</td> // .home-events-date
					<td class="home-events-title" itemprop="name">
						<?php echo strlen(get_the_title()) > 35 ? substr(get_the_title(), 0, 35) . '...' : get_the_title(); ?>
					</td>
					<td class="home-events-location" itemprop="location" itemscope itemtype="http://schema.org/Place">
						<span itemprop="addressLocality"><?php the_event_city(); ?></span>, <span itemprop="addressRegion"><?php the_event_state(); ?></span>
					</td>
					<td class="home-events-more-info">
						<?php
							if( tribe_get_event_website_url() && tribe_event_in_category('heroes-6k') )
							{
								$link = tribe_get_event_website_url();
							}
							else
							{
								$link = get_the_permalink();
							}
						?>
						<a href="<?php echo $link; ?>" itemprop="url">Learn More</a>
					</td>
				</tr>
			<?php 
			} //endforeach 

			wp_reset_postdata(); 
			?>			
		</table>
	</section> //#home-events
</div>/.row 
-->
<!--
	// Featured Hero
-->
<div id="featured-hero-row" class="row">
	<section id="featured-hero" class="col-sm-12">
		<div id="featured-hero-copy" class="col-sm-9">
			<h2 class="section-h2">Featured Hero</h2>
			<h3><?php echo get_field('featured_hero_name'); ?></h3>
			<p><?php echo get_field('featured_hero_description'); ?></p>
			<a href="<?php echo get_field('featured_hero_link'); ?>" title="Continue" class="hir-btn hir-btn-blue">Continue</a>
		</div><!-- /#featured-hero-copy -->
		<div id="featured-hero-img" class="col-sm-3">
			<img src="<?php echo get_field('featured_hero_image'); ?>" alt="<?php echo get_field('featured_hero_name'); ?>">
		</div><!-- /#featured-hero-img -->
	</section><!-- /#featured-hero -->
</div><!-- /#featured-hero-row -->
<!--
	// Who We Are
-->
<div id="who-we-are-row" class="row">
	<section id="who-we-are" class="col-sm-10 col-sm-offset-1">
		<div class="row">
			<div class="col-sm-6">				
				<?php echo get_field('video_embed'); ?>
			</div>
			<div class="col-sm-6">
				<h2 class="section-h2">Who We Are</h2>
				<p><?php echo get_field('who_we_are_copy'); ?></p>
				<a href="<?php echo get_field('who_we_are_link'); ?>" title="More" class="hir-btn">More</a>
			</div>
		</div><!-- /.row -->
	</section><!-- /#who-we-are -->
</div><!-- /#who-we-are-row -->
<?php get_footer(); ?>