<?php
/**
 * Template Name: 6k Race
 */
get_header();
?>
<?php get_template_part('_page-top'); ?>
<div class="tab-nav-row row">
	<nav class="tab-nav col-sm-10 col-sm-offset-1">
		<ul>
			<li><a href="#about" title="About" class="tab-link active" data-target="about">About</a></li>
			<li><a href="#charity" title="Charity" class="tab-link" data-target="charity">Charity</a></li>
			<li><a href="#volunteer" title="Volunteer" class="tab-link" data-target="volunteer">Volunteer</a></li>
			<li><a href="#course" title="Course" class="tab-link" data-target="course">Course</a></li>
			<li><a href="#schedule" title="Schedule" class="tab-link" data-target="schedule">Schedule</a></li>
			<li><a href="#awards" title="Awards &amp; Results" class="tab-link" data-target="awards">Awards &amp; Results</a></li>
			<li><a href="#photos" title="Photos" class="tab-link" data-target="photos">Photos</a></li>
		</ul>
	</nav><!-- /.tab-nav -->
</div><!-- /.row -->
<div class="page-row row" itemscope itemtype="http://schema.org/Event">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="tabbed-content">
			<!--
				// About Us
			-->
			<div id="tab-1" class="tab-wrap" data-tab="about">
				<div class="row">
					<div class="col-sm-4">
						<div class="row">
							<div class="col-sm-3 mobile-full">
								<?php
									$month = date('F', strtotime(get_field('date')));
									$day = date('d', strtotime(get_field('date')));
									$year = date('Y', strtotime(get_field('date')));
								?>								
								<div class="race-date-wrap" itemprop="startDate" content="<?php echo $year . '-' . $month . '-' . $day; ?>">
									<div class="race-month"><?php echo $month; ?></div>
									<div class="race-day"><?php echo $day; ?></div>
									<div class="race-year"><?php echo get_field('start_time'); ?></div>
								</div>
							</div>
							<div class="col-sm-9 mobile-full" itemprop="location" itemscope itemtype="http://schema.org/Place">
								<!-- <span class="race-charity"><?php //echo get_field('charity_name'); ?></span><br> -->
								<span class="race-location" itemprop="address"><?php echo get_field('location'); ?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<?php if(get_field('display_poster_or_map') == 'Poster'): ?>
								<div class="race-poster">
									<img src="<?php echo get_field('poster'); ?>" itemprop="image">
								</div>
								<?php else: ?>
								<div class="race-map" itemprop="hasmap">
									<?php echo get_field('map_code'); ?>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-sm-12">
								<img src="<?php echo get_bloginfo('template_url'); ?>/img/6k.png" id="race-logo">
							</div>
						</div>
					</div>
					<div class="col-sm-8 race-description" itemprop="description">
						<?php echo get_field('description'); ?>
					</div>
					<div class="col-sm-12 sponsor-wrap">
						<p>Sponsors</p>
						<div class="sponsors">
							<?php
							if( have_rows('sponsors') ):
								while ( have_rows('sponsors') ) : the_row();
									echo '<img src="'.get_sub_field('sponsor_logo').'">';
								endwhile;
							endif;
							?>
						</div>
						<div class="sponsors-copy">
							<?php echo get_field('sponsor_copy'); ?>
						</div><!-- /.sponsors-copy -->						
					</div>
				</div>
			</div><!-- /#tab-1 -->

			<!--
				// Charity
			-->
			<div id="tab-1" class="tab-wrap" data-tab="charity">
				<?php echo get_field('charity_copy'); ?>
			</div><!-- /#tab-1 -->			

			<!--
				// Volunteer
			-->
			<div id="tab-1" class="tab-wrap" data-tab="volunteer">
				<?php echo get_field('volunteer_copy'); ?>
			</div><!-- /#tab-1 -->	

			<!--
				// Course
			-->
			<div id="tab-1" class="tab-wrap" data-tab="course">
				<?php echo get_field('course_copy'); ?>
			</div><!-- /#tab-1 -->	

			<!--
				// Schedule
			-->
			<div id="tab-1" class="tab-wrap" data-tab="schedule">
				<?php echo get_field('schedule_copy'); ?>
			</div><!-- /#tab-1 -->

			<!--
				// Awards
			-->
			<div id="tab-1" class="tab-wrap" data-tab="awards">
				<?php echo get_field('awards_copy'); ?>
			</div><!-- /#tab-1 -->

			<!--
				// Photos
			-->
			<div id="tab-1" class="tab-wrap" data-tab="photos">
				<?php echo get_field('gallery_shortcode'); ?>
			</div><!-- /#tab-1 -->			
		
		</div><!-- /.tabbed-content -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>