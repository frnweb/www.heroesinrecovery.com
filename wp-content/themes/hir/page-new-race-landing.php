<?php
/**
 * Template Name: 2018 6k Race Landing
 */

get_header();
?>

<?php //get_template_part('_page-top'); ?>
<div class="green-heroes-row row new-6k-bg">
	<div class="mw-wrap">
		<div class="col-sm-12 col-md-4" id="blue-heroes">				
			<h4><?php echo get_field('heroes_sponsor_title'); ?></h4>
			<?php echo get_field('heroes_sponsor_copy'); ?><a href="<?php echo get_field('file_sponsor'); ?>"><button class="sponsor-orange-cta"><?php echo get_field('sponsor_button_text'); ?></button>
			</a>
		</div><!-- /#submit-content -->	
		<div class="col-sm-12 col-md-4" id="heroes-get-social">				
			<h4><?php echo get_field('heroes_get_social_title'); ?></h4>
			<p><?php echo get_field('heroes_get_social_copy'); ?></p>
		</div><!-- /heroes-get-social -->
		<div class="col-sm-12 col-md-4" id="meet-race-team">				
			<h4><?php echo get_field('heroes_meet_race_title'); ?></h4>
			<?php echo get_field('heroes_meet_race_copy'); ?>
			<a href="<?php echo get_field('heroes_meet_race_link'); ?>" class=""><button class="sponsor-orange-cta"><?php echo get_field('heroes_meet_race_link_copy'); ?></button></a>
		</div><!-- /meet-race-team -->	
	</div>	<!-- /.mw-wrap -->
</div><!-- /.row -->
<div class="orange-heroes-row row nu-6k-red hidden-xs hidden-sm">
	<div class="mw-wrap">
		<div class="col-sm-12 col-md-6" id="">	

			<?php echo get_field('facebook'); ?>			

		</div><!-- /meet-race-team -->
		<div class="col-sm-12 col-md-6" id="">				
			<?php echo get_field('instagram'); ?>		
		</div><!-- /heroes-get-social -->
	</div><!-- /.mw-wrap -->
</div><!-- / orange row -->

<div class="page-row row partners-row">
	<div class="mw-wrap">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="row page-content-row" align="center">
					<h2>Your Participation Benefits These Charities</h2>	
						<?php if(get_field('all_charity_logos')): ?>

						<?php while(has_sub_field('all_charity_logos')): ?>
							<div class="partners_logo">
							<div class="size_protector">
							<a href="<?php the_sub_field('charity_link'); ?>">
							<img class="author_image" src="<?php the_sub_field('charity_logos'); ?>">
							</a>
							</div>
							</div>

						<?php endwhile; ?>

					<?php endif; ?>
						</div><!-- /row page content row -->
					</article>
					</div><!-- /.mw-wrap -->
</div><!-- / partners row -->


<!-- / Begin Script for Facebook Live Feed -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- / End Script for Facebook Live Feed -->


<?php get_footer(); ?>