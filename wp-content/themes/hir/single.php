<?php get_header(); ?>
<?php get_template_part('searchform'); ?>
<div class="page-row row">
	<div class="col-sm-8 col-sm-offset-2">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('full-blog-post'); ?>>				
				<div class="full-blog-post-content">
					<h1 class="single-title"><?php the_title(); ?></h1>
					<div class="single-meta">
						<div class="single-meta-inner">
							<div class="single-meta-image"><?php echo get_avatar( get_the_author_meta( 'email' ), 600, '', get_the_author_meta( 'display_name' ) ); ?></div>
							<div class="single-meta-author-wrap"><div class="single-meta-author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="author"><?php the_author_meta( 'display_name' ); ?></a> | 
							<span style="white-space:nowrap;"><?php the_date(); ?></span></div></div>
						</div><!-- /.single-meta-inner -->
					</div><!-- /.single-meta -->
					<div class="single-content">
						<?php the_content(); ?>
					</div>
					<?php comments_template(); ?>	
				 	<div class="single-pagination row">
				 		<div class="single-pagination-prev col-sm-6">
				 			<?php previous_post_link('%link', '&laquo; Previous Post'); ?>
				 		</div><!-- /.single-pagination-prev -->
				 		<div class="single-pagination-next col-sm-6">
				 			<?php next_post_link('%link', 'Next Post &raquo;'); ?>
				 		</div><!-- /.single-pagination-next -->
				 	</div><!-- /.single-pagination -->
				</div><!-- /#full-blog-post-content -->				
			</article><!-- /.full-blog-post -->		
		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article <?php post_class('full-blog-post'); ?>>
				<h2 class="full-blog-post-title">Nothing to Display</h2>
			</article>
			<!-- /article -->

		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>