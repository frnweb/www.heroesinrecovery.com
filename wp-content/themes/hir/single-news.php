<?php get_header(); ?>
<?php get_template_part('searchform'); ?>
<div class="page-row row">
	<div class="col-sm-8 col-sm-offset-2">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('full-blog-post'); ?>>				
				<div class="full-blog-post-content">
					<div class="row single-story-top">
						<div class="col-sm-4 single-story-image">
							<?php the_post_thumbnail('hero-image'); ?>
						</div><!-- /.single-story-image -->
						<div class="col-sm-8 single-story-meta">
							<div class="single-story-meta-inner">
								<div class="single-story-hero">
									<?php
									if(get_post_meta(get_the_ID(), 'hir_hero_story_name', true))
									{
										echo get_post_meta(get_the_ID(), 'hir_hero_story_name', true);
									}
									elseif(get_field('hero_name'))
									{
										echo get_field('hero_name');
									}
									?>
								</div><!-- /.single-story-hero -->
								<h1 class="single-story-title"><?php the_title(); ?></h1>
								<div class="single-story-date"><?php the_date(); ?></div>
							</div>
						</div><!-- /.single-story-meta -->
					</div><!-- /.row -->
					<div class="single-content">
						<?php the_content(); ?>
					</div>
					<div>
						<a href="<?php echo get_bloginfo('url'); ?>/about/?tab=news">View All News</a>
					</div>
					<?php comments_template(); ?>	
				 	<div class="single-pagination row">
				 		<div class="single-pagination-prev col-sm-6">
				 			<?php previous_post_link('%link', '&laquo; Previous Post'); ?>
				 		</div><!-- /.single-pagination-prev -->
				 		<div class="single-pagination-next col-sm-6">
				 			<?php next_post_link('%link', 'Next Post &raquo;'); ?>
				 		</div><!-- /.single-pagination-next -->
				 	</div><!-- /.single-pagination -->
				</div><!-- /#full-blog-post-content -->				
			</article><!-- /.full-blog-post -->		
		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article <?php post_class('full-blog-post'); ?>>
				<h2 class="full-blog-post-title">Nothing to Display</h2>
			</article>
			<!-- /article -->

		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>