<?php
/**
 * Template Name: Stories Archive
 */
?>
<?php get_header(); ?>
<?php get_template_part('_page-top'); ?>
<?php get_template_part('searchform'); ?>
<div class="row story-archive-row">	
	<!-- Ajax Posts Here -->
	<?php
	$search = get_query_var('s');
    $a      = $_GET['a'];

    // Build Shortcode
    $code = '[ajax_load_more post_type="stories" scroll="true" posts_per_page="18" button_label="Load More"';

    if(!$a)
    {        
        $code .= ' search="'.$search.'"]';
    }
    else
    {
         $code .= ' meta_key="hir_hero_story_name" meta_value="'.$search.'" meta_compare="LIKE"]';
    }

	?>
	<?php echo do_shortcode($code); ?>
</div><!-- /.story-archive-row -->
<?php get_footer(); ?>