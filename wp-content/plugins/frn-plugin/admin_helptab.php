<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );


//////
// Creating a Help tab
//

function my_plugin_help($contextual_help, $screen_id, $screen) {

	global $frn_settings_page;
	$screen = get_current_screen();
	
	// Add help tabs
	if ( $screen->id != $frn_settings_page ) return;
		
	$phone_help = '
	<br />
	<h2>Phone Number Shortcode Help</h2>
	<div class="typical_box">
		<table class="frn_help_table"><tr>
			<th colspan="2">Auto Scan Feature</th>
			</tr><tr>
				<td valign="top" nowrap><b>Overview</b></td>
				<td valign="top"><div class="shortcode_indent">This features uses PHP or JavaScript to scan elements of a page for a phone number pattern. If found, it automatically puts our typical &lt;SPAN&gt; code around the number. From there our typical phones2links.js (placed in the footer) will take over and turn the numbers into links for mobile devices.
					<p>Phone numbers must have a space, opening bracket, or less than (&lt;) before it and a period, space, closing bracket, or greater than (&gt;) after it to be found. This reduces the chances that a group of digits in the middle of external links would be considered as phone numbers and having our SPAN tag added to the middle of it (and really messing up the page). Whatever characters or space is around the phone number will also be linked visually in mobile devices. But those characters will not be in the tel: phone number portion of the link for mobile devices. That makes sure the numbers are still dialable.</p></div></td>
			</tr><tr>
				<td valign="top" nowrap><b>Disabled</b></td>
				<td valign="top"><div class="shortcode_indent">Default setting. Turns off all phone number scanning on the site.</div></td>
			</tr><tr>
				<td valign="top" nowrap><b>Only Content Fields</b></td>
				<td valign="top"><div class="shortcode_indent">This scans the content prior to looking for shortcodes. You shouldn\'t have to worry about double span codes and links. Only works on Page and Post content fields. It will not scan any other part of a page. No title, no widgets, no header or footer.</div></td>
			</tr><tr>
				<td valign="top" nowrap><b>Only Widgets</b></td>
				<td valign="top"><div class="shortcode_indent">There may be a case where you have added shortcodes to pages/posts but not widgets and have too many to look through. This option turns on scanning the content of widgets only. It will not scan widget titles or any other part of a page.</div></td>
			</tr><tr>
				<td valign="top" nowrap><b>Only Widgets & Content</b></td>
				<td valign="top"><div class="shortcode_indent">This option activates the scanning of both widgets and pages or posts content fields. See notes for the other two options above for more detail on how each option works.</div></td>
			</tr><tr>
				<td valign="top" nowrap><b>Entire Page</b></td>
				<td valign="top"><div class="shortcode_indent">This option will not be helpful if you have already used shortcodes or our frn_phones SPAN code on the site. If you have, this feature will shut down on pages where that SPAN code exists. It\'s possible this may affect things visually, functionally, and how things are recorded in Analytics.<br /> <br />
					This option turns off the PHP scanning of content fields and, for mobile devices only, relies on a JavaScript scan of all code between BODY tags. It will look at everything on the page--header, footer, widgets, content--every bit of code and link the phone numbers. This code is in the phones2links.js in the footer. When this setting is selected, it adds another &lt;SCRIPT&gt; line just above the phones2links.js in the footer. That activates a variable that the phones2links.js code looks for to activate the entire page scanning. Phones2links.js already scans the BODY for SPANs normally for both computers and mobile devices. But when this option is activated, it scans for phone number patterns instead and on mobile devices only and changes them to links if found.</div></td>
			</tr></table>
			<h3>Auto Scan Recognized patterns</h3>
			<p>
			<b>International:</b><br />
			0000 0000<br />
			00 00 00 00<br />
			00 000 000<br />
			00000000<br />
			00 00 00 00 00<br />
			+00 0 00 00 00 00<br />
			00000 000000<br />
			+00 0000 000000<br />
			(00000) 000000<br />
			+00 0000 000000<br />
			+00 (0000) 000000<br />
			00000-000000<br />
			00000/000000<br />
			000 0000<br />
			000-000-000<br />
			0 0000 00-00-00<br />
			(0 0000) 00-00-00<br />
			0 000 000-00-00<br />
			0 (000) 000-00-00<br />
			000 000 000<br />
			000 00 00 00<br />
			000 000 000<br />
			000 000 00 00<br />
			+00 00 000 00 00<br />
			0000 000 000<br />
			(000) 0000 0000<br />
			(00000) 00000<br />
			(0000) 000 0000<br />
			0000 000 0000<br />
			0000-000 0000<br />
			0000 000 0000<br />
			00000 000000<br />
			0000 000000<br />
			0000 000 00 00<br />
			+00 000 000 00 00<br />
			(000) 0000000<br />
			+00 00 00000000<br />
			000 000 000<br />
			+00-00000-00000<br />
			(0000) 0000 0000<br />
			+00 000 0000 0000<br />
			(0000) 0000 0000<br />
			+00 (00) 000 0000<br />
			+00 (0) 000 0000<br />
			+00 (000) 000 0000<br />
			(00000) 00-0000<br />
			(000) 000-000-0000<br />
			(000) [00]0-000-0000<br />
			(00000) 0000-0000<br />
			+ 000 0000 000000</p>
			
			<p><b>U.S. Versions:</b><br />
			0 (000) 000-0000<br />
			+0-000-000-0000<br />
			0-000-000-0000<br />
			000-000-0000<br />
			(000) 000-0000<br />
			000-0000<br />
			0 (000) 000-0000 ext 1<br />
			0 (000) 000-0000 x 1001<br />
			0 (000) 000-0000 extension 2<br />
			0 000 000-0000 code 3
			</p>
	</div>
	<div class="typical_box">
		<table class="frn_help_table"><tr>
			<th colspan="2">Common Shortcodes for Text Numbers</th>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help1" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help1\')">[frn_phone ga_phone_location="Phone Clicks in <span style="color:red;"><b>##location on page##</b></span>"]</span></b></td>
				<td valign="top"><div class="shortcode_indent">Displays phone number with typical SPAN around it for mobile and Google Analytics tracking.</div></td>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help2" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help2\')"><&#63;php echo do_shortcode(\'[frn_phone ga_phone_location="Phone Clicks in <span style="color:red;"><b>##location on page##</b></span>"]\'); &#63;></span></b></td>
				<td valign="top"><div class="shortcode_indent">Use for header.php or footer.php or inserting into any php file. Use a typical shortcode within the single quotes.</div></td>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help3" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help3\')">[frn_phone only="yes"]</span></b></td>
				<td valign="top"><div class="shortcode_indent">Use when you only want a phone number to show without formatting or mobile tracking. Used best with images since we need SPANs put around the image or an object. The following code will also work in places shortcodes won\'t.<br />
					Example Use: <b><span id="frn_phone_help4" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help4\')">&lt;span id="frn_phones" ga_phone_location="Phone Clicks in <span style="color:red;"><b>##location on page##</b></span>" frn_number="<span style="color:red;"><b>[frn_phone only="yes"]</b></span>"&gt;&lt;img src="[your image address]" alt="[Don\'t forget Alt tag]" /&gt;&lt;/span&gt;</span></b></div></td>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help9" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help9\')">[frn_phone number="###-###-####"]</span></b></td>
				<td valign="top"><div class="shortcode_indent">Add number="" anytime the frn_phone shortcode is used with any other attributes if you want to use a unique number for that location and override the site\'s default. Whatever phone number format you use here will be displayed as is on the page surrounded by our SPAN JavaScript code. Example: [frn_phone number="555.555.1234" ga_phone_location="Phone Clicks in Sidebar"]</div></td>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help5" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help5\')">[frn_phone]</span></b></td>
				<td valign="top"><div class="shortcode_indent">Use this only in content pages like posts and pages. The Google Analytics tracking code for this is "Phone Clicks (General)". Using this only for in-page phone numbers helps total them all together in reporting since we expect those clicks to be very few. But for pages where it\'s important to know the location of the phone number in stats, then use the option above where you can set a location label.</div></td>
			</tr><tr>
			<th colspan="2" style="text-align:left;padding-top:30px;font-size:16px;">Common Shortcodes for Phone Numbers in Images</th>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help6" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help6\')">[frn_phone ga_phone_location="Phone Clicks in ##page location##" image_url="" alt="" title="" id="" class="" css_style=""]</span></b></td>
				<td valign="top"><div class="shortcode_indent">This shortcode will build the SPAN code BUT ALSO it will build the image HTML between the SPAN code. This example shortcode includes all possible attributes available in the shortcode. Attributes can be added in any order. They don\'t have to be in the order you see to the left within the brackets. Remove any you don\'t need or leave them empty as they are.</div></td>
			</tr><tr>
				<td valign="top"><b><span id="frn_phone_help7" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help7\')">%%frn_phone%%</span></b></td>
				<td valign="top"><div class="shortcode_indent">This will add the phone number to any of our shortcodes. It\'s most helpful when adding to image Alt or Title tags. Since shortcodes normally start with a bracket, WordPress only processes the first shortcode, not the second one within it. Therefore, we just created our own shortcode approach to use within our own shortcodes using two percents (%%).</div></td>
		</tr><tr>
				<td valign="top"><b><span id="frn_phone_help8" class="frn_shortcode_sel" onClick="selectText(\'frn_phone_help8\')"></span></b></td>
				<td valign="top"><div class="shortcode_indent"></div></td>
		</tr></table>
	</div>
	
	<table class="frn_help_table">
		<tr>
			<th>Customizable Attributes for Text Uses:</th>
		</tr>
		<tr>
			<td valign="top">
				<li><b><span id="phone_shortcode_help1" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help1\')">ga_phone_location="Phone Clicks in ##page location##</span></b>  --- Most often customized. This is what we use to know what phone numbers on the site are clicked on. When this is not in a shortcode, the default is: "Phone Clicks (General)"</li>
				<li><b><span id="phone_shortcode_help2" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help2\')">ga_phone_category=""</span></b>  --- The main category that all our phone number clicks are stored under in Google Analytics. Default: "Phone Numbers"</li>
				<li><b><span id="phone_shortcode_help3" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help3\')">ga_phone_label=""</span></b> --- Can be used to store a phone number if they are different on a site. Simply put "include_phone" between the quotes and it\'ll automatically store the phone number in Analytics (e.g. <strong><span id="phone_shortcode_help3b" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help3b\')">ga_phone_label="include_phone"</span></strong>). The default is "Calls" and serves as another way to group the data in Analytics.</li>
				<li><b><span id="phone_shortcode_help4" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help4\')">frn_number_style="white-space:nowrap;"</span></b> --- If you want to style a number for only mobile devices, use this. Default:  "white-space:nowrap" … "style=" is added before it automatically … To remove any mention of styles, put "none" between parenthesis.</li>
				<li><b><span id="phone_shortcode_help5" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help5\')">only="yes"</span></b> --- Use if you want only the phone number to show. Default format for this is 1-555-555-1212, the format that best works for iPhones. No Google Analytics tracking or mobile linking will be included.</li>
				<li><b><span id="phone_shortcode_help6" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help6\')">int_prefix=""</span></b> --- Used only by Rehab and Treatment since the international portion is separate from the number generated by IfByPhone scripts. This makes sure the international code is used in the mobile phone link and passed to the LHN slideout tab as part of the number.</li>
				<li><b><span id="phone_shortcode_help7" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help7\')">lhn_tab_disabled="yes"</span></b> --- Use if you want to disable the phone number where the shortcode is used not to be included in the LHN slide out tab. If you have a shortcode or frn_phone SPAN code somewhere else on the page with out a disabled option or in the main FRN Settings, then there could still be a number in the slideout. This is really helpful if you have different numbers on a page and want to control which one shows in the slideout. It does not matter if yes is used or not. If you don\'t want it disabled, you must remove the variable from the shortcode or SPAN.</li>
			</td>
		</tr><tr>
			<th style="text-align:left;padding-top:30px;font-size:16px;">Customizable Attributes for Image Uses</th>
		</tr><tr>
			<td valign="top">
				<li><b><span id="phone_shortcode_help11" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help11\')">image_url=""</span></b> --- This is the key piece that triggers the image code to be built. This should be the URL to the image file on the server.</li>
				<li><b><span id="phone_shortcode_help12" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help12\')">alt=""</span></b> --- Fill this out to include alt text in the image. You only need the text. The shortcode will take care of adding "alt=".</li>
				<li><b><span id="phone_shortcode_help13" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help13\')">title=""</span></b> --- Fill this out if you want a text bubble to show for the image. It can be that last push to sell a person to click. The shortcode will take care of adding "title=".</li>
				<li><b><span id="phone_shortcode_help14" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help14\')">id=""</span></b> --- If you have an id CSS style you want applied to the image. Just enter the ID here. E.g. id="image_id"</li>
				<li><b><span id="phone_shortcode_help15" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help15\')">class=""</span></b> --- If you have a class CSS style you want applied to the image. Just enter the ID here. E.g. class="image_class"</li>
				<li><b><span id="phone_shortcode_help16" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help16\')">css_style=""</span></b> --- If you want to directly add a style to the image, enter the portion that would be between the quotes in a normal style attribute. E.g. css_style="border:0;margin:5px;"</li>
			</td>
		</tr>
	</table>
	<table class="frn_help_table">
		<tr>
			<th>"Location" Labeling for Analytics:</th>
		</tr>
		<tr>
			<td valign="top">
			Use the following "###location###" labels depending on where you want to place a phone number. Following this labeling will help us when looking at data in Google Analytics.<br />
			<div class="typical_box">
				<li><b>Header:</b> "<span id="phone_shortcode_help6" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help6\')">Phone Clicks in Header</span>"</li>
				<li><b>Footer:</b> "<span id="phone_shortcode_help7" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help7\')">Phone Clicks in Footer</span>"</li>
				<li><b>Sidebar:</b> "<span id="phone_shortcode_help8" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help8\')">Phone Clicks in Sidebar (Just Ask Widget)</span>"</li>
				<li><b>Content:</b> "<span id="phone_shortcode_help9" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help9\')">Phone Clicks in Page (Last Paragraph)</span>"</li>
				<li><b>Images:</b> "<span id="phone_shortcode_help10" class="frn_shortcode_sel" onClick="selectText(\'phone_shortcode_help10\')">Phone Clicks in Page (Banner)" or "Phone Clicks in Header (Banner)</span>"</li>
			</div></td>
		</tr>
	</table>
	<br />
	
	';
		
		
	$screen->add_help_tab( array(
		'id'    => 'frn_tab_phone',
		'title' => __( 'Phone Shortcode', 'frn_phone_help' ),
		'content' => __( $phone_help, 'frn_phone_help'
		),
	)
	);

	$screen->add_help_tab( array(
		'id'    => 'frn_tab_ga',
		'title' => __( 'Google Analytics', 'frn_ga_help' ),
		'content'   => __( '
			<br />
			<h2>Help for Google Analytics Options</h2>
			<p>Most of the activation options are not available for Universal Analytics. Be sure to click the question mark icon next to each item to see the specifics.</p>
			<h3>General Information</h3>
			<p>The system automatically turns off Google Analytics for us admins. It will display a note in the header like "YOU ARE LOGGED IN and therefore not being tracked in Google Analytics or the FRN setting for the GA ID is empty". If the page is missing a Google Analytics ID, then another message will display on the page clarifying that. If you want to override the defaults and force the Google code to show for everyone visiting the site, place a checkmark in the box for "Test Mode".</p> <p>This code tracks click events on downloads, emails, 404 errors, and outbound links. As a result, it also automatically adds a target=_blank to all links that do not match the root domain for the site. For example, if the site has a www at the front of the page where the link is, but the link does not have www even though it\'s to another page on the same site, this code will still think it\'s an outbound link. Someday we\'ll code an exception, but for now this is how it\'ll function until it becomes an issue.</p>
			<h3>404 Page Titles</h3>
			<p>The default for the 404 page title is "Page Not Found". That\'s the WordPress default. However, some themes come with a 404 page template and sometimes they change it to "Nothing found for..." or something similar. Enter the beginning words of the page title for the 404 page--the words between the &lt;title&gt; tags at the top of the page. If you don\'t include this, then details on 404 errors will not be tracked--only that a 404 error occurred. The 404 event tracking in Analytics will tell us what page they were trying to visit when they got the error. We can then try to fix those broken link periodically.</p>
			<h3>Universal Analytics</h3>
			<p>This is Google\'s new way of integrating Analytics into a site. You must convert this site\'s Analytics account to <a href="https://developers.google.com/analytics/devguides/collection/upgrade/guide" target="_blank" >Universal Analytics</a> via the account\'s admin in the Property settings BEFORE activating the option in the plugin below. Activating this will switch the GA code to this new version. To activate demographics in Universal Analytics, you need to go to the site\'s Property settings  (<a href="https://www.google.com/analytics/web/?hl=en#management/Settings/a33689825w61035006p62464467/%3Fm.page%3DPropertySettings/" target="_blank">FRN Events example</a>). Once Google lets you know it\'s complete, you can then select this option in the plugin below. Activating this will switch the GA code to this new version. For us, there are no additional features we\'ll use when compared to the classic version other than maybe a lighter page. It mostly helps with Adwords and if we provide logins on our sites (as of 4/15/14).</p>
			<h3>DoubleClick Demographics</h4>
			<p>This affects the privacy policy. To activate this feature using Universal Analytics, go to the site\'s Property settings  (<a href="https://www.google.com/analytics/web/?hl=en#management/Settings/a33689825w61035006p62464467/%3Fm.page%3DPropertySettings/" target="_blank">FRN Events example</a>). This feature uses DoubleClick advertising data and what they assume about a person as they visit sites on the web. The old version pulls Google\'s Analytics JS code from DoubleClick servers instead of Google\'s. Universal Analytics works differently. Learn more: <a href="https://support.google.com/analytics/answer/2819948?hl=en" target="_blank" >Activating Demographics</a></p>
			<h3>Enhanced Link Attribution</h3>
			<p>Enhanced link attribution starts tracking where links are clicked on a page. Normally when looking at in-page analytics, if more than one link goes to the same page, they would all have the same numbers (i.e. pageviews). Activating this will tell Google to track how many clicks each individual link on a page actually gets. It\'s best if you are planning on optimizing a webpage on the site. Otherwise, keep it off. Activating this will not apply to data in the past--only going forward. Learn more: <a href="https://support.google.com/analytics/answer/2558867?hl=en" target="_blank" >Enhanced link attribution</a></p>
			', 'frn_ga_help'
		),
	) );
	
	$screen->add_help_tab( array(
		'id'    => 'frn_tab_lhn',
		'title' => __( 'LiveHelpNow', 'frn_lhn_help' ),
		'content'   => __( '
			<br />
			<h2>LHN Shortcode</h2>
			<p>The shortcode allows us to place basic code anywhere in a page, header, footer, or widget and WordPress will automatically replace it with whatever we tell it to. It acts like a variable we can make happen or change at any time.</p>
			<p>Remember that you set the site-wide chat and email button defaults here on this page below. When you do so, all you need to add to a page is the basic shortcodes (e.g. [lhn_inpage button="chat"]). Only use the id or url attributes when you want to customize the buttons on one particular page.</p>
			<div class="typical_box">
				<h3>Shortcode Options:</h3>
				<table class="customizable_options"><tr>
				<td><li>Both Buttons All Defaults:</li>
					<li>Only the Chat Button:</li>
					<li>Customized Chat Button:</li>
					<li>Only the Email Button:</li>
					<li>Customized Email Button:</li></td>
				<td><li><b><span id="lhn_shortcode_help1" class="frn_shortcode_sel" onClick="selectText(\'lhn_shortcode_help1\')">[lhn_inpage]</span></b> (or [lhn_inpage button="both"])</li>
					<li><b><span id="lhn_shortcode_help2" class="frn_shortcode_sel" onClick="selectText(\'lhn_shortcode_help2\')">[lhn_inpage button="chat"]</span></b></li>
					<li><b><span id="lhn_shortcode_help3" class="frn_shortcode_sel" onClick="selectText(\'lhn_shortcode_help3\')">[lhn_inpage button="chat" id="####"]</span></b></li>
					<li><b><span id="lhn_shortcode_help4" class="frn_shortcode_sel" onClick="selectText(\'lhn_shortcode_help4\')">[lhn_inpage button="email"]</span></b></li>
					<li><b><span id="lhn_shortcode_help5" class="frn_shortcode_sel" onClick="selectText(\'lhn_shortcode_help5\')">[lhn_inpage button="email" url="http://mylink.com/image.png"]</span></b></li></td>
				</tr></table>
			</div>
			<br />
			<p><b>Directions for Each Button:</b> Use the [lhn_inpage] shortcode to display the default LHN buttons on a page side-by-side (or [lhn_inpage button="both"]). It will add a style to the page with display:inline and puts a div id="lhn_buttons" around both buttons. Use [lhn_inpage button="email"] or [lhn_inpage button="chat"] to show only the respective buttons and control their layout. The chat button will wrap to its own line unless you add a &lt;div&gt; around the button shortcode and then create a style with a sub &lt;div&gt; and make it display:inline (similar to the default when the [lhn_inpage] is used by itself.</p>
			<p><b>Customize Chat Button:</b> To customize a chat button, upload your button image to the <a href="https://www.livehelpnow.net/lhn/console/admin/install_live_chat_button/code1.aspx?style=30" target="_blank">main LHN website</a> that stores the online/offline button images. You\'ll and add an id="####" to the shortcode (replacing the #### with the button id).</p>
			<p><b>Customize Email Button:</b> To use a custom email button, add email_url="" to the shortcode. Put the full HTTP web address to the image in between the quotes. Only this button can be changed to a <b>text</b> link instead of a button. Use text="email" with button="email" in the shortcode to activate the option (e.g. <b>[lhn_inpage button="email" text="email"]</b>). You can put anything you want in the quotes. Just something needs to be present to change from a button to text.
			<p><b>System Defaults:</b> Leaving the button id and email url fields blank will default to our standard LHN buttons. Selecting to deactivate the buttons here will turn them off everywhere on the site where the shortcode is used.</p>
			<br />
			<br />
			', 'frn_lhn_help'
		),
	) );
	
	$screen->add_help_tab( array(
		'id'    => 'frn_tab_ftr',
		'title' => __( 'Footer Elements', 'frn_ftr_help' ),
		'content'   => __( '
			<br />
			<h2>Footer Section Options</h2>
			This section includes settings for both the privacy policy URL and Chartbeat tracking code.
			<h3>Privacy Policy URL Shortcode</h3>
			Since we treat privacy policy styling and location so differently, the only element generally consistent was the URL. If we decide that we want to be more consistent, we can create another shortcode that represents more code. Leaving the URL field blank will use the default URL that points to the policy included with our plugin. If you have a custom version for this site, enter the URL for its location here. It will automatically change the URL for all privacy shortcodes used on the site. You can have a special privacy policy just for one page by adding "url" to the shortcode and using a full URL to the privacy policy. It will override all defaults (e.g. [frn_privacy_url url="http://website.com/privacy.html"]).<br />
			<br />
			<div class="typical_box">
				Shortcode: <b><span id="priv_shortcode_help2" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help2\')">[frn_privacy_url url="[optional]"\']</span></b><br />
				Footer.php: <b><span id="priv_shortcode_help3" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help3\')">&lt;?php echo do_shortcode("[frn_privacy_url]"); ?&gt;</span></b>
			</div>
			<br />
			<h3>Default Footer Shortcode</h3>
			<p>The standard footer format for FRN is:</p> 
			<p><strong>&lt;p style="text-align:center;"&gt;Copyright © '.date("Y").' [site name]. All Rights Reserved. | Confidential and Private Call: 877.714.1318 | Privacy Policy&lt;/p&gt;</strong></p>
			<p><strong>Activate Auto Footer:</strong> You can automatically add a footer to the wp_footer() template location on a page simply by placing a check in the Activate Auto Footer checkbox. Wp_footer() needs to be in the location in footer.php where you want the copyright to show. However, any plugins and default WordPress JavaScript code also goes into the wp_footer() location. In most cases, the copyright info will show above any scripts added, but since we can\'t control third party plugins, their scripts may affect spacing and possibly styling if they add CSS into the footer area. Also, if you keep the shortcode in footer.php and have the Activate Auto Footer checked, you will have two footers showing on the page. You will need to remove the shortcode from footer.php or deactivate the auto feature checkbox. </p>
			<p><strong>Shortcode Options</strong>: By using the shortcode below, a version like that will be added to the footer incorporating the FRN Settings. Like normal, you can customize pieces of the shortcode to keep it dynamic and to make it easier to update all sites with the FRN plugin. You can change the alignment, start year, copyright name, phone number, privacy policy url, phone number styling, and Google Analytics event category and labels. If you don\'t want to customize an element, just don\'t use the variable in the example shortcode below. Here is a list of all of the variables (example use of the variables are in the box below): <br />
				<ul>
					<li style="margin-bottom:0;"><span id="priv_shortcode_help4" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help4\')" style="font-weight:bold;">align=""</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help5" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help5\')" style="font-weight:bold;">startyear=""</span> (If not used, then no year range will be used in footer)</li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help6" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help6\')" style="font-weight:bold;">sitename=""</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help7" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help7\')" style="font-weight:bold;">frn_phone=""</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help8" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help8\')" style="font-weight:bold;">frn_privacy_url=""</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help9" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help9\')" style="font-weight:bold;">ga_phone_category=""</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help10" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help10\')" style="font-weight:bold;">ga_phone_location=""</span> (default is "Phone Clicks in Footer")</li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help11" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help11\')" style="font-weight:bold;">ga_phone_label=""</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help12" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help12\')" style="font-weight:bold;">frn_number_style=""</span> (refer to the phone number shortcode for options)</li>
					<li style="margin-top:0;"><span id="priv_shortcode_help13" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help13\')" style="font-weight:bold;">paragraph="no"</span> (use "no" if you don\'t want P tags around footer)</li>
					<li style="margin-top:0;"><span id="priv_shortcode_help13b" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help13b\')" style="font-weight:bold;">nodiv="yes"</span> (use "yes" or "y" if you don\'t want the div tags around the footer)</li>
				</ul></p>
			<p><br />
				<strong>Using a Customized Footer</strong>: You can also use your own footer code but keep the year, name, phone number and privacy policy url dynamic so you only have to change those in one place. Whatever you add there will automatically be added to the normal footer locations (using the auto footer or shortcode). An example of what you could use in the footer textbox below would be: </p>
				<span id="priv_shortcode_help14a" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help14a\')" style="font-weight:bold;margin-left:30px;">&lt;div&gt;Copyright 2006-%%year%% %%site_name%% | Let us help. %%frn_phone%% | &lt;a href="%%frn_privacy_url%%"&gt;Privacy Policy&lt;/a&gt;&lt;/div&gt;</span>
				<p>You must use %% or square brackets around variables to have them replaced by the default settings. The only features lost in this version is the ability to customize the phone number font formatting. You\'ll have to modify the actual CSS file to affect formatting. All other normal shortcode parameters listed above will still affect the dynamic elements you add below. This mostly helps with the Google Analytics event category and labels. The full list of dynamic elements you can add to your code are: 
				<ul>
					<li style="margin-bottom:0;"><span id="priv_shortcode_help14" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help14\')" style="font-weight:bold;">%%year%%</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help15" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help15\')" style="font-weight:bold;">%%sitename%%</span></li>
					<li style="margin-top:0;margin-bottom:0;"><span id="priv_shortcode_help16" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help16\')" style="font-weight:bold;">%%frn_phone%%</span> (Analytics label will be "Phone Clicks in Footer")</li>
					<li style="margin-top:0;"><span id="priv_shortcode_help17" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help17\')" style="font-weight:bold;">%%frn_privacy_url%%</span></li>
				</ul></p>
			<br />
			<div class="typical_box">
				Shortcode: <b><span id="priv_shortcode_help18" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help18\')" style="font-weight:bold;">[frn_footer startyear="optional" sitename="optional" frn_phone="optional" privacy_url="optional" ga_phone_location="optional" \']</span></b><br />
				Footer.php: <b><span id="priv_shortcode_help19" class="frn_shortcode_sel" onClick="selectText(\'priv_shortcode_help19\')" style="font-weight:bold;">&lt;?php echo do_shortcode(\'[frn_footer start_date="optional" site_name="optional" frn_phone="optional" privacy_url="optional"]\'); ?&gt;</span></b>
			</div>
			<br />
			<h3>Chartbeat Options</h3>
			<p>To test to make sure Chartbeat is working, select the "test" radio button. You can then view the source code for the site and make sure things are filled out correctly. Select "activate" to make sure that company employees are not tracked.</p>
			<p>Chartbeat code is automatically disabled for people logged into the site or accessing it from the company IP addresses (206.19.211.16 and 173.164.20.3; <b>your IP is '.$_SERVER['REMOTE_ADDR'].'</b>).</p>
			<p>To add a category or change the name of it, we can easily do that by using the plugin and then rolling it out to all the sites. We could use a function to automatically reset the category to something else for, let\'s say all the "niche" sites. But it\'s like no matter how we slice it, we\'ll have to go back to the specific sites to select the new category. Renaming is not challenging either. In the end, adding a category is easy. It\'s the choosing the category that needs to be manual.</p><br /> <br />
			
			', 'frn_ftr_help'
		),
	) );
	
	$screen->add_help_tab( array(
		'id'    => 'frn_social_info',
		'title' => __( 'Social Help', 'frn_social_help' ),
		'content'   => __( '
			<br />
			<h2>Social Shortcode Top Notes</h2>
			<ul>
				<li><b>[twitter][/twitter]</b> This shortcode is similar to frn_social, but already includes options activated for our typical use of the twitter button instead of the icon. You can use just the shortcode [twitter] or the bookends approach. Only use the bookends version when you want to use text on the page for the tweet. Otherwise, include the "tweet" attribute to change what\'s used in the Twitter popup window. Using the bookends causes the button to align right automatically. And due to WordPress changing HTML you enter, any text you\'ve wrapped with these shortcodes will automatically have paragraph tags placed around them--thereby adding additional spacing above and below. If you don\'t provide tweet text or have text wrapped, then it\'ll use the article title. As of 6/5/18, alignment doesn\'t work. If you need it to, you\'ll need to resort to the frn_social settings and variables below. You can wrap images or text with this, but only text will be used in the tweet.</li>
				<li><b>[frn_social][/frn_social]</b> You can now put the normal [frn_social] shortcode before an image (or it\'s link) and then add [/frn_social] after it. This will automatically wrap the image and social shortcode together so you don\'t have to worry about adding a DIV code with the appropriate float CSS option. NOTE: As of 1/30/18, this feature will only work when an image is within the wrapping shortcodes. It will not work when you only wrap text. It comes with it\'s own CSS name for you to control styling if you\'d like. The sharing options will automatically be placed after the image. If the image is linked, be sure the code is inside the shortcode bookends. When inserting an image and you use WP\'s alignment dropdown, that setting will also be used for the wrapping DIV. Otherwise, you\'ll need to include the "div_align" attribute in the shortcode at the front. e.g. <b>div_align="right"</b></li>
				<li>All Buttons: The shortcode in the social section at the bottom allows you to just place one shortcode for all three buttons and customize as needed. This reduces duplication and helps with positioning when all three are used. Not all attributes are needed. There are defaults for all of them except bold, tweet, image, and summary.</li>
				<li>PreText: You can add text before the buttons or icons. It works best with float="inline". Use pretext="Share This:" in the shortcode. You can make the text anything you\'d like.</li>
				<li>Default Twitter options use the page\'s URL for the shared url after your message followed by the username and "Tweet It" for the linked text on the page. Cross promoted/related Twitter usernames will be suggested after the person sends their tweet if they don\'t follow the account.</li>
				<li>Default Twitter button options use the page\'s URL for the shared url, "Tweet" for the button, and the medium sized button. Everything else if blank will not be in the code. Using a float left or float right will help line up the button vertically if used in line with text--although it will then be thrust to the front or end of the line. Consider adding a &lt;div&gt; around it to help.</li>
				<li>If you don\'t want a button showing in the list of icons, add the label for it and make it equal no (e.g. linkedin="no"). You cannot customize the title, image, or shared page URL for buttons.</li>
				<li>Although, this shortcode will work in all the places our other shortcodes work, only use this text in widget, post, or page content due to the code including links and JavaScript.</li>
				<li>To get analytics on all of our sites that use the ShareThis button, go to <a href="http://sharethis.com/publishers/social-analytics.php#mainNav:nav-left-list-trends" target="_blank">ShareThis Social Analytics</a> and login with the FRN account. The twitter link and button will be tracked in Google Analytics. If you add a Facebook or Google+ button using the code found on their sites, Analytics will pick up sharing activity there too.</li>
				<li>Pinterest: The counter default is none. But you can use "above" or "beside" if you want the counter to show. To use the larger button, use version="large". To use just the round icon, use version="icon" or circular. To change the button to red or white, just use color="red" or white in the shortcode. To use a button that allows the person to select any article image on the page, just use [frn_social type="PINTEREST"]. </li>
			</ul>
			<h2>All Attributes Available in Shortcode (all are optional)</h2>
			<p>The following is likely duplicate from other places in Help. But in case you are looking for specific attributes, this will make it easier to find them. Try searching the page for them as well to see them in context of the other buttons.</p>
			<ul>
				<li><b>type</b>: Default is our top four social networks. This determines if you want all (leave blank) or if you want to show a specific button. The order attribute can be used similarly.</li>
				<li><b>order</b>: Default is pftl. Simply use the first letter of the social network to get it to show and move the letters around to get the order you like.</li>
				<li><b>version</b>: Default is icons. Use icon or button. Twitter provides a text option. Just use "text" instead of the other two. It will not work for other buttons.</li>
				<li><b>float</b>: Default is float left. Older attribute friendly for those who work with CSS often. Use float="none" if you want the icons to be vertical.</li>
				<li><b>align</b>: Default is left. Newer attribute that overrides float.</li>
				<li><b>div_align</b>: Default is whatever the "align" attribute is unless this attribute is added. This controls the wrapping DIV around the social buttons.</li>
				<li><b>style</b>: This will pass the CSS styles you include to the wrapping DIV\'s STYLE attribute (e.g. style="margin-left:10px;"). The wrapping DIV and each button has it\'s own CSS in case you want to style it via the site\'s CSS style sheet. Use the CSS parent/child relationship to be specific.</li>
				<li><b>pretext</b>: Adds text in front of the social buttons. Works best when align="inline".</li>
				<li><b>size</b>: Changes the size of the buttons and icons. Default is small. You can try adding pixels, but often the size is just small or large.</li>
				<li><b>place</b>: This helps with positioning on top of images at the top or bottom. It\'s a little buggy. Play around with it if you wish. You can use negative pixels (-15px).</li>
				<li><b>counter</b>: Default is none. Only works when you have version="button" and only if the default social network offers a counter.</li>
				<li><b>url</b>: Default is the current page\'s URL. You can override this when version="icon" if you are sharing an image from another page.</li>
				<li><b>tweet</b>: (Twitter only) This has a default. See above. This is the tweet text provided automatically when someone clicks the icon or button.</li>
				<li><b>hashtag</b>: (Twitter only) Default is #addiction. Change this to what you want. There is no way to remove the hashtag.</li>
				<li><b>link_text</b>: (Twitter only) Default is "Tweet This". If you just want to use Twitter, you can use a text version of the button in addition to the icon or button. Version must equal "text" for this to activate.</li>
				<li><b>account</b>: (Twitter only) There is no default for this. If you want to specify an account that a Tweet came from, include this attribute with the Twitter username without an @ symbol.</li>
				<li><b>related</b>: (Twitter only) Default is HeroesNRecovery. This allows us to feature another Twitter account when people read the person\'s tweet about our post. They can\'t remove this feature. If you want to promote another Twitter account, include its username here without the @ symbol.</li>
				<li><b>related_tagline</b>: (Twitter only) Default is "Challenging the Stigma". This only happens when a related account is provided. If you provide another related account, you\'ll want to change this too. It can\'t be blank.</li>
				<li>The following is for ShareThis Buttons Only:<ul>
					<li><b>image</b>: (Applies to Pinterest Also) There is no default. If you want the social network to specifically show an image, include the full URL for it in this attribute.</li>
					<li><b>bold</b>: Default is the post\'s title. This is the bolded section when you have use version="sharethis" or included an "s" in the order. There is no guarentee that the social network will use this, but it could help provide meaning to the person trying to share our content.</li>
					<li><b>summary</b>: Default is the post\'s Meta description. This is the description under the bolded portion when you have use version="sharethis" or included an "s" in the order. There is no guarentee that the social network will use this, but it could help provide meaning to the person trying to share our content.</li>
					<li><b>like</b>: See ShareThis section below.</li>
					<li><b>youtube_url</b>: See ShareThis section below.</li>
					<li><b>plusone</b>: See ShareThis section below.</li>
					<li><b>google</b>: See ShareThis section below.</li>
					<li><b>facebook</b>: See ShareThis section below.</li>
					<li><b>twitter</b>: See ShareThis section below.</li>
					<li><b>linkedin</b>: See ShareThis section below.</li>
					<li><b>email</b>: See ShareThis section below.</li>
					<li><b>pinterest</b>: See ShareThis section below.</li>
				</ul></li>
			</ul>
			<ul>
				<li><b>Common for All Buttons:</b> 
					<ul>
						<li><b>type:</b> Provide this if you want to just use one sharing button. If you want more than one, add the ORDER attribute instead.</li>
						<li><b>div_align:</b> Include this if you are using the bookend approach of the shortcode. Use only "left" or "right" and both the image and shortcode options will align appropriately as so. The normal "align" attribute only applies to the sharing buttons. They will be aligned within the DIV around block. e.g. <b>div_align="right"</b></li>
						<li><b>version:</b> Use "text","icon", or leave blank. Controls if you want text to be linked and optionally rely on CSS to style it, or if you want to use our custom round icon, or if you want to use the standard version provided by the social network.</li>
						<li><b>order:</b> Used to display specific social accounts and control their order. Just use the first initial of the network. Our default set of four uses "order=pftl. Others letters would be "g" for Google+, "e" for email, "y" for youtube, and "s" for ShareThis.</li>
						<li><b>align:</b> Options: left, right, center, and none. Use this to help control spacing/margins. Spacing will change depending on your alignment choice. The DIV wrapping the buttons will also change. You can control some of this via CSS and your own HTML around the shortcode if you want. Providing none should produce the same results as not including the attribute.</li>
						<li><b>style</b>: This will pass the CSS styles you include to the wrapping DIV\'s STYLE attribute (e.g. style="margin-left:10px;"). The wrapping DIV and each button has it\'s own CSS in case you want to style it via the site\'s CSS style sheet. Use the CSS parent/child relationship to be specific.</l
						<li><b>link_text:</b> Only applies if you use version="text". Whatever you put here will be the text linked on the page. Do not use HTML with quotes.</li>
						<li><b>size:</b> This varies with the button you\'re using. But you can try small, medium, or large. If that doesn\'t work, provide a pixel value.</li>
						<li><b>place:</b> Used rarely. This will position the sharing buttons on top of the object you want to be shared--similar to the way Pinterest does their button.</li>
						<li><b>url:</b> This is the URL to be shared. This does not apply to every option. But if you use a text or icon version, you can provide a specific URL to share in case you are promoting another page.</li>
						<li><b>counter:</b> This only applies if you are using the social network\'s default sharing button. By default, this system disables the counter. To get it to, just add "yes" to this attribute. This option doesn\'t apply to every button--but it does to most.</li>
					</ul>
					<br />
				</li>
				<li><b>Twitter Specific (type="TWITTER"):</b> 
					<ul>
						<li><b>tweet:</b> This is the tweet you want to automatically be provided to the person who wants to tweet about the image or quote. Automatically added to it will be our FRN twitter username and hashtag. If you don\'t like the default, add those attributes and leave them blank.</li>
						<li><b>account:</b> By default, this uses the Heroes twitter account as the promoted account. But you can use any twitter username here you\'d like.</li>
						<li><b>related:</b> By default, this promotes the Heroes twitter account when anyone tweets out a page. You can use any twitter username here you\'d like.</li>
						<li><b>related_tagline:</b> By default, this is "Challenging the Stigma". Along with a related account, you can provide a tagline that will show with it to help describe it. </li>
						<li><b>hashtag:</b> By default, we use "#addiction". This is added to the tweet and will take away from the 160 character limit. Add the attribute and leave it blank if you don\'t want it included.</li>
					</ul>
					<br />
				</li>
				<li><b>ShareThis Specific (type="SHARETHIS"):</b> 
					<ul>
						<li><b>bold:</b> You\'ll see bolded text in the ShareThis window. This changes it. If you don\'t provide this, it\'ll likely use the page\'s title.</li>
						<li><b>summary:</b> This is the descriptive text for the page. It\'ll grab from the content itself, but you can use it more like a teaser to get people to click through--instead of a summary.</li>
						<li><b>image:</b> Many pages have more than one image on it. Providing the URL for the image here will make sure it\'s the one featured in the ShareThis window. Once shared on a social network, it may or may not show there still. But this increases the changes.</li>
						<li><b>like:</b> Use "like=no" if you don\'t want the Facebook "like" button to show in the ShareThis box. </li>
						<li><b>google:</b> Use "google=no" if you don\'t want this option to show in the ShareThis box. ShareThis provides two Google sharing option. This is not the version for Google+.</li>
						<li><b>plusone:</b> Use "plusone=no" if you don\'t want this option to show in the ShareThis box. ShareThis provides two Google sharing option. This is the one for the +1 button.</li>
						<li><b>facebook:</b> Use "facebook=no" if you don\'t want this option to show in the ShareThis box.</li>
						<li><b>twitter:</b> Use "twitter=no" if you don\'t want this option to show in the ShareThis box.</li>
						<li><b>linkedin:</b> Use "linkedin=no" if you don\'t want this option to show in the ShareThis box.</li>
						<li><b>email:</b> Use "email=no" if you don\'t want this option to show in the ShareThis box.</li>
						<li><b>pinterest:</b> Use "pinterest=no" if you don\'t want this option to show in the ShareThis box.</li>
					</ul>
					<br />
				</li>
				<li><b>Youtube_url:</b> Used only when you want the youtube icon or text to show (type=youtube or order=y with version=text or icon). This can be any web address you want. This merely helps you put a youtube option with the rest of the social networks although it\'s not technically sharing a link.</li>
				
			</ul>
			<p><br />
				You can get fully customized ShareThis code by logging in and building your own buttons. There are dozens to choose from. </p>
				<p><b>IMPORTANT</b>: When adding buttons to content after first clicking the "text" tab and then switch to the "visual" tab, WordPress likes to remove variables in spans that are not normal HTML variables. ShareThis is foremost affected, but sometimes the others are as well. So, use with caution.<br />
				<li><a href="http://www.sharethis.com/get-sharing-tools/">Go to ShareThis.com</a></li>
			</p>
			<p>You can get fully customized Twitter code:<br />
				<li><a href="https://about.twitter.com/resources/buttons">Twitter Buttons</a></li>
			</p>
			<p>You can get fully customized Pinterest code:<br />
				<li><a href="http://business.pinterest.com/en/widget-builder#do_pin_it_button">Pinterest Buttons</a></li>
			</p>
			<p>You can get fully customized Facebook buttons:<br />
				<li><a href="https://developers.facebook.com/docs/plugins/like-button">Like Buttons</a></li>
				<li><a href="https://developers.facebook.com/docs/plugins/share-button">Share Buttons</a></li>
				<li><a href="https://developers.facebook.com/docs/plugins/follow-button">Follow Buttons</a></li>
			</p>
			<p>You can get fully customized Goolge+ buttons:<br />
				<li><a href="https://developers.google.com/+/web/share/">Google+ Buttons</a></li>
			</p>
			', 'frn_social_help' )
	) );
	
	

	
	// Set help sidebar
	$screen->set_help_sidebar(
		'
		<h3>Updating the Plugin</h3>
		This is not in the WordPress plugins store. You must use FTP to drag over the non-zipped files or use ManageWP to upload a new zip file that contains all of the plugin files. You cannot just upload a zip file via the Plugins section of WordPress since it will not let you overwrite the files. Contact Daxon Edwards if you find and error or need help (615-400-2064).
		<ul>
			<li></li>
		</ul>
		'
	);
	
}
if ( is_admin() ) add_filter('contextual_help', 'my_plugin_help', 10, 3);