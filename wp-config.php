<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JnQRLCOv7zMLEO28P1kZsZ1ZKPlKhyROiykSbXzmc/NC3C5jNLZ219chboVG9PuX2d/9sqr6VCeeo9/0FcasWw==');
define('SECURE_AUTH_KEY',  '65q2raXpxapS2yUqhzEj/hrHkJ4p8Pashg/z9mPqyQKK+GY0e6fzvL6XxQ6UEGhArpRsSwzPF8HMkCU8Ii7qgw==');
define('LOGGED_IN_KEY',    'mQvcidMh5Anq0VQN1Tm/LOQEfJD8uy/6Pf21knHWE57y/sMfqUs7YGf2Wcdb3Fpr8XPAckfMYYR0tm+Cr9YZKQ==');
define('NONCE_KEY',        'hVDRmJby4Uy3hjjOfFMRANIuNHl4n7o2i3Wjrq89FnvrloHw82mEODC5AGQwTYCFrAfKZI0tQd4q8IE1PN6JrA==');
define('AUTH_SALT',        '7HgiQktOHP/LpOEP5B+dHxC+twBeura7tRCTcK1GqBVF4606/pKqclYl7SLOsKHUMq9eu0hmeTbUlsqWLpd6Iw==');
define('SECURE_AUTH_SALT', '5E7IXaB4Eaeb/jVrQnGJwmU3JwXNqLbMPl0CiYBLaZwmcot40CnEBX6LyOeyFP8Z8ySLCvVwrzBodNaOkAWw0w==');
define('LOGGED_IN_SALT',   'Jpqon6+5r2udw9BAyD1odO7w6EnlogbS/cmoWwP78oGYEs2hpZfKVX+z07QI4dnCfRj9SiHe/KTFcPHFWkF1Bw==');
define('NONCE_SALT',       'ZYyj1Rs5T6Y/PWjY/RWk0XmwLKJUpaaIUceg8g7BjYeSbuWHX/QecrGw/ugEBtZOWBjq7Xvh/As3h6ECyBQUug==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_vkmra6xgen_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
