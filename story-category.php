<?php 

require('./wp-blog-header.php');
header("HTTP/1.1 200 OK");

global $cfs;

$category = $_GET["catName"];

$args = array(
	'post_type'		 => 'stories',
	'posts_per_page' => 9,
	'orderby'		 => 'rand',
	'tax_query' 	 => array(
						array(
							'taxonomy' => 'story_category',
							'field' => 'slug',
							'terms' => ''. $category .'',
							'operator' => 'IN'
							)	
						)
);

//print_r($args);

?>

<div id="story-info">
	<!--
	<div class="sort">
		<form method="post" action="/stories/">
			sort by: 
			<select name="sort" id="sort" onchange="this.form.submit();">
				<option value="date"<?php if ($order=='date') echo ' selected="selected"'; ?>>most recent</option>
				<option value="title"<?php if ($order=='title') echo ' selected="selected"'; ?>>story name</option>
			</select>
		</form>
	</div>
	-->
	<p><?php echo ucwords( str_replace('-', ' ', $category) ); ?></p>
</div>

<?php

if ($category == 'all stories') { query_posts('post_type=stories&posts_per_page=9&orderby=rand'); } else { query_posts($args); }

$p=0; 

while (have_posts()) : the_post(); 

	$p++;

	if ($p==5): //Center box ?>

		<div class="story-box promo">
					<a href="/share/">
						<div class="story-photo"><img src="/wp-content/themes/heroes/images/story-share.png" /></div>
						<div class="story-hero" style="background-color:#5c7f9f; font-family:Georgia, 'Times New Roman', serif; color:#FFFFFF; text-align:center; font-style:italic;"><p style="padding:17px 0 0 0; margin:0; font-size:16px;">Be an Inspiration ></p></div>
					</a>
		</div>
		
	<?php else: ?>
	
		<div class="story-box">

			<a href="<?php the_permalink(); ?>">
			<?php $terms = wp_get_post_terms( $post->ID, 'story_category' ); ?>
			<div class="story-photo">
				<?php if ( has_post_thumbnail() ) : the_post_thumbnail(array(235,235)); endif; ?>
			</div>
			<div class="story-hero" style="background-color:<?php echo $terms[0]->description; ?>;">
				<p><span><?php echo $cfs->get('hero_name', $post->ID); ?></p>
			</div>
			</a>
		</div>
	
	<?php endif; ?>

<?php endwhile; ?>

<div id="story-nav">
	<?php if(function_exists('wp_paginate')) {
		wp_paginate();
	} ?>
</div>